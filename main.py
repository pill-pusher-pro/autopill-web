import sys
import logging
import argparse
import threading
import multiprocessing

from flask_utils.run import FlaskRunner, get_main_path

import config

class AutopillRunner(FlaskRunner):
    _svc_name_ = 'Autopill'
    _svc_display_name_ = 'Autopill'
    _svc_description_ = 'Web interface for autopill'

    want_elevate = False
    ports = (8898,)
    filters = {'GET':{'', 'livelog'}}

def main():
    global log
    svc = AutopillRunner()

    p = argparse.ArgumentParser(description='')
    p.add_argument('-s', '--serial', default='/dev/ttyACM0', help='serial port to open')
    svc.add_args(p)

    args = p.parse_args()

    if svc.check_args(args):
        return

    svc.config_log()

    svc.log_filter('GET', 'api')

    config.load_config()

    from led_gpio import define_led, open_all, blink_thread
    import flask_main
    import autopill_interface
    import flask_autopill
    import flask_livelog
    import flask_ui
    import flask_db
    import flask_system

    log = logging.getLogger(__name__)

    rled = define_led('red', 12)
    led = define_led('yellow', 16)
    led = define_led('green', 20)
    led = define_led('blue', 21)

    open_all()
    rled.set(False)

    thr = threading.Thread(target=blink_thread, args=(), daemon=True)
    thr.start()

    intf = autopill_interface.AutopillInterface(args.serial)
    autopill_interface.AutopillInterface.instance = intf
    intf.start()

    svc.run_flask(args, flask_main.app)

if __name__ == '__main__':
    multiprocessing.freeze_support()
    main()
