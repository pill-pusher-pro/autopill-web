import sys
import os
import re
import time
import datetime
import functools
import sqlite3
import json
import logging

from pathlib import Path

from ktpanda import object_pool
from ktpanda.sqlite_helper import SQLiteDB, in_transaction, Query

from utils import parsedate, BASEPATH
from date_filter import parse_filter, compile_filter

DATE_EPOCH = datetime.date(2020, 1, 1)

DBPATH = BASEPATH / 'autopill.sqlite'

log = logging.getLogger(__name__)

SCHEMA_VERSION = 7
SCHEMA = r'''
CREATE TABLE IF NOT EXISTS drug (
    drugid INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL UNIQUE,
    ndc TEXT UNIQUE,  -- 11-digit NDC without dashes
    strength INTEGER, -- micrograms
    hopper_index INTEGER NULL UNIQUE,
    notes TEXT NOT NULL DEFAULT '',
    active INTEGER NOT NULL DEFAULT 1,
    count INTEGER NOT NULL DEFAULT 0,
    reorder_point INTEGER NOT NULL DEFAULT -1
) STRICT;;

CREATE INDEX IF NOT EXISTS drug_reorder ON drug(count) WHERE count <= reorder_point;;

CREATE TABLE IF NOT EXISTS user (
    userid INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL UNIQUE,
    active INTEGER NOT NULL DEFAULT 1,
    led INTEGER NULL

) STRICT;;

CREATE TABLE IF NOT EXISTS schedule (
    scheduleid INTEGER PRIMARY KEY AUTOINCREMENT,
    userid INTEGER NOT NULL REFERENCES user(userid),
    drugid INTEGER NOT NULL REFERENCES drug(drugid),
    active INTEGER NOT NULL DEFAULT 1,
    start_date INTEGER NOT NULL,
    time INTEGER NOT NULL,
    last_dispense INTEGER NOT NULL DEFAULT 0,
    count INTEGER NOT NULL DEFAULT 1,
    date_filter TEXT NOT NULL DEFAULT ''
) STRICT;;

CREATE TABLE IF NOT EXISTS dispense_history (
    timestamp INTEGER,
    userid INTEGER NOT NULL REFERENCES user(userid),
    drugid INTEGER NOT NULL REFERENCES drug(drugid),
    count INTEGER NOT NULL
) STRICT;;

CREATE INDEX IF NOT EXISTS dispense_history_time ON dispense_history(timestamp);

'''.split(';;')

def withdb(func):
    def wrapper(*a, **kw):
        object_pool.reap_old_objects(3600)

        with object_pool.checkout(AutopillDB, DBPATH) as db:
            db.connect()
            return func(db, *a, **kw)

    functools.update_wrapper(wrapper, func)
    return wrapper

class AutopillDB(SQLiteDB):
    # Raspberry Pi might get unplugged - use full synchronization
    PRAGMA_synchronous = 'FULL'
    common_schema = SQLiteDB.common_schema + SCHEMA
    schema_version = SCHEMA_VERSION
    check_same_thread = False

    def _upgrade_to_2(self, oldvers):
        self.backend.execute('ALTER TABLE user ADD COLUMN active INTEGER NOT NULL DEFAULT 1')

    def _upgrade_to_3(self, oldvers):
        self.backend.execute('ALTER TABLE drug ADD COLUMN active INTEGER NOT NULL DEFAULT 1')

    def _upgrade_to_4(self, oldvers):
        self.backend.execute('ALTER TABLE user ADD COLUMN led INTEGER NULL')

    def _upgrade_to_5(self, oldvers):
        self.backend.execute('ALTER TABLE drug ADD COLUMN count INTEGER NOT NULL DEFAULT 0')

    def _upgrade_to_6(self, oldvers):
        self.backend.execute('ALTER TABLE drug ADD COLUMN reorder_point INTEGER NOT NULL DEFAULT -1')


def _db_update_default(db, key, val, id, existing, new_fields):
    return val

def _db_update_datefilter(db, key, val, id, existing, new_fields):
    if val:
        try:
            parse_filter(val)
        except Exception as e:
            raise ValueError(f'{key}: invalid filter: {e}')
    return val

def _db_update_bool(db, key, val, id, existing, new_fields):
    intv = int(val)
    if intv not in {0, 1}:
        raise ValueError(f"{key}: invalid bool: {val!r}")
    return intv

def _db_update_ndc(db, key, val, id, existing, new_fields):
    if not val:
        return None

    m = re.match(r'(\d{4,5})-?(\d{3,4})-?(\d{1,2})', val)
    if not m:
        raise ValueError(f"invalid NDC: {val!r}")

    new_ndc = (
        m.group(1).rjust(5, '0') +
        m.group(2).rjust(4, '0') +
        m.group(3).rjust(2, '0')
    )
    if len(new_ndc) > 11:
        raise ValueError(f"invalid NDC: {val!r}")
    return new_ndc

def _db_update_date(db, key, val, id, existing, new_fields):
    if isinstance(val, datetime.date):
        return (val - DATE_EPOCH).days

    if isinstance(val, str):
        dt = parsedate(val)
        return (dt - DATE_EPOCH).days

    return val

def _db_update_time(db, key, val, id, existing, new_fields):
    if isinstance(val, str):
        hours = minutes = -1
        if ':' in val:
            parts = [int(v) for v in val.split(':')]
            hours = parts[0]
            minutes = parts[1]
        elif len(val) > 2:
            hours = int(val[:-2])
            minutes = int(val[-2:])
        else:
            hours = int(val)
            minutes = 0

        if not (0 <= hours <= 23 and 0 <= minutes <= 59):
            raise ValueError(f'{key}: invalid time: {val!r}')

        return hours * 60 + minutes

    return val

def datetext(v):
    return (DATE_EPOCH + datetime.timedelta(days=v)).isoformat()

def timetext(v):
    hours = v // 60
    minutes = v % 60
    return f'{hours:02d}:{minutes:02d}'

@in_transaction
def _save_object(db, cls, objid, new_fields):
    existing = None
    if objid is not None:
        existing = cls.QUERY_BY_ID.run(db, objid)
        if not existing:
            return None
        existing = existing[0]

    args = {'objid': objid}
    update_fields = []

    for key, validate in cls.UPDATE_FIELDS.items():
        if key in new_fields:
            val = validate(db, key, new_fields[key], objid, existing, new_fields)
            args[key] = val
            update_fields.append(key)

    try:
        if existing:
            set_fields = ",".join(f'{field} = ${field}' for field in update_fields)
            db.execute(f'UPDATE {cls.TABLE} SET {set_fields} WHERE {cls.ID_FIELD} = $objid', args)
        else:
            insert_fields = ",".join(update_fields)
            values_fields = ",".join('$' + field for field in update_fields)
            curs = db.execute(f'INSERT INTO {cls.TABLE}({insert_fields}) VALUES({values_fields})', args)
            objid = curs.lastrowid

    except sqlite3.IntegrityError as e:
        clashes = []
        for field in cls.UNIQUE_FIELDS:
            if field in args:
                other_objid = db.query_scalar(f'SELECT {cls.ID_FIELD} FROM {cls.TABLE} WHERE {field} = ?', (args[field],))
                if other_objid is not None and other_objid != objid:
                    clashes.append((field, args[field], other_objid))
        if not clashes:
            raise
        return {
            cls.ID_FIELD: objid,
            'error': 'Conflicts occurred',
            'conflicts': clashes
        }

    return cls.get(db, objid).fields

class DBOMeta(type):
    def __new__(mcs, name, bases, typedict):
        ncls = super().__new__(mcs, name, bases, typedict)
        base = ncls.QUERY_BASE
        if base:
            if 'QUERY_BY_ID' not in typedict:
                ncls.QUERY_BY_ID = base.copy().where(f'{ncls.ID_FIELD} = ?').build()
            if 'QUERY_ACTIVE' not in typedict:
                ncls.QUERY_ACTIVE = base.copy().where(f'pri.active = 1').build()

        return ncls

class DataObject(metaclass=DBOMeta):
    TABLE = ''
    ID_FIELD = 'id'
    QUERY_BASE = None
    QUERY_BY_ID = None
    QUERY_ACTIVE = None
    UPDATE_FIELDS = {}
    UNIQUE_FIELDS = ()

    def __init__(self, fields):
        self.fields = fields
        self.id = fields.get(self.ID_FIELD)
        self._calcfields()

    def _calcfields(self):
        pass

    def __getattr__(self, k):
        try:
            return self.fields[k]
        except KeyError:
            raise AttributeError(k) from None

    @classmethod
    def list(cls, db, include_inactive=False, **filters):
        dbargs = []
        filters = [(k, v) for k, v in filters.items() if v is not None]

        query = cls.QUERY_BASE if include_inactive else cls.QUERY_ACTIVE
        if filters:
            query = query.copy()
            for key, val in filters:
                clause = cls.FILTER_FIELDS[key]
                query.where(clause)
                dbargs.append(val)
            query.build()

        rows = query.run(db, *dbargs)

        return [cls(fields) for fields in rows]

    @classmethod
    def get(cls, db, id):
        rows = cls.QUERY_BY_ID.run(db, id)
        if rows:
            return cls(rows[0])
        return None

    @classmethod
    def put(cls, db, id=None, new_fields=None):
        try:
            return _save_object(db, cls, id, new_fields)
        except (ValueError, SyntaxError) as e:
            return {
                'error': str(e)
            }

    def save(self, db, fields=None):
        if fields is None:
            new_fields = self.fields
        else:
            new_fields = {key: self.fields[key] for key in fields}

        self.put(db, self.id, new_fields)

class Drug(DataObject):
    TABLE = 'drug'
    ID_FIELD = 'drugid'
    FILTER_FIELDS = {
        'ndc': 'pri.ndc = ?',
        'hopper_index': 'pri.hopper_index = ?'
    }
    QUERY_BASE = (
        Query().from_('drug', 'pri')
        .add('pri', 'drugid', 'name', 'ndc', 'strength', 'hopper_index', 'active',
             'notes', 'count', 'reorder_point')
        .build())

    UNIQUE_FIELDS = 'name', 'ndc', 'hopper_index'
    UPDATE_FIELDS = {
        'name': _db_update_default,
        'strength': _db_update_default,
        'hopper_index': _db_update_default,
        'ndc': _db_update_ndc,
        'active': _db_update_bool,
        'notes': _db_update_default,
        'count': _db_update_default,
        'reorder_point': _db_update_default,
    }

    def _calcfields(self):
        self.fields['active'] = bool(self.fields.get('active'))

DRUG_NEED_REORDER = (
    Drug.QUERY_BASE.copy()
    .where('count <= reorder_point')
    .order('count')
    .build()
)

def drugs_needing_reorder(db):
    return DRUG_NEED_REORDER.run(db)

@in_transaction
def adjust_drug_count(db, drugid, count):
    db.execute('UPDATE drug SET count = MAX(0, count + ?) WHERE drugid = ?', (count, drugid))
    return Drug.get(db, drugid)

class User(DataObject):
    TABLE = 'user'
    ID_FIELD = 'userid'
    QUERY_BASE = (
        Query().from_('user', 'pri')
        .add('pri', 'userid', 'name', 'active', 'led')
        .build())

    UNIQUE_FIELDS = 'name',
    UPDATE_FIELDS = {
        'name': _db_update_default,
        'active': _db_update_bool,
        'led': _db_update_default,
    }

    def _calcfields(self):
        self.fields['active'] = bool(self.fields.get('active'))

class Schedule(DataObject):
    TABLE = 'schedule'
    ID_FIELD = 'scheduleid'
    FILTER_FIELDS = {
        'userid': 'pri.userid = ?',
        'drugid': 'pri.drugid = ?',
    }
    QUERY_BASE = (
        Query().from_('schedule', 'pri')
        .add('pri', 'scheduleid', 'userid', 'drugid', 'count', 'last_dispense', 'start_date', 'time', 'date_filter', 'active')
        .build())

    UNIQUE_FIELDS = ()
    UPDATE_FIELDS = {
        'start_date': _db_update_date,
        'time': _db_update_time,
        'date_filter': _db_update_datefilter,
        'userid': _db_update_default,
        'drugid': _db_update_default,
        'count': _db_update_default,
        'last_dispense': _db_update_date,
        'active': _db_update_bool,
    }

    def _calcfields(self):
        self.fields['active'] = bool(self.fields.get('active'))
        self.fields['last_dispense_iso'] = datetext(self.fields['last_dispense'])
        self.fields['start_date_iso'] = datetext(self.fields['start_date'])
        self.fields['time_text'] = timetext(self.fields['time'])

class ScheduleExpand(Schedule):
    QUERY_BASE = (
        Schedule.QUERY_BASE.copy()
        .join('user', 'u', 'userid = pri.userid')
        .join('drug', 'd', 'drugid = pri.drugid')
        .addas('u', 'name', 'user_name')
        .addas('d', 'name', 'drug_name')
        .add('d', 'hopper_index')
        .add('u', 'led')
        .build())

PENDING_SCHEDULES = (
    ScheduleExpand.QUERY_BASE.copy()
    .where('pri.active = 1')
    .where('u.active = 1')
    .where('d.active = 1')
    .where('d.hopper_index NOTNULL')
    .where('(last_dispense < $date - 1) OR (last_dispense < $date AND time <= $time)')
    .build())

@in_transaction
def reset_schedules(db, dt, scheduleid=None):
    today = dt.date()
    datenum = (today - DATE_EPOCH).days
    timeofday = dt.hour * 60 + dt.minute

    sql = (
        'UPDATE schedule '
        'SET last_dispense = (CASE WHEN time >= $time THEN $datenum ELSE $datenum - 1 END) '
        'WHERE last_dispense >= $datenum'
    )

    if scheduleid is not None:
        sql += ' AND scheduleid = $scheduleid'

    db.execute(sql, {'time': timeofday, 'datenum': datenum, 'scheduleid': scheduleid});

def get_pending_schedules(db, dt):
    today = dt.date()
    yesterday = today - datetime.timedelta(days=1)
    datenum = (today - DATE_EPOCH).days
    timeofday = dt.hour * 60 + dt.minute
    result = []

    for res in PENDING_SCHEDULES.run(db, date=datenum, time=timeofday):
        if res['date_filter']:
            try:
                func = compile_filter(res['date_filter'])
                compdate = today if timeofday >= res['time'] else yesterday
                if not func(compdate):
                    continue
            except Exception:
                log.exception(f'Exception running filter for schedule {res["scheduleid"]}')
                continue
        result.append(res)

        res['active'] = bool(res.get('active'))
        res['last_dispense_iso'] = datetext(res['last_dispense'])
        res['start_date_iso'] = datetext(res['start_date'])
        res['time_text'] = timetext(res['time'])

    return result

@in_transaction
def add_dispense_history(db, timestamp, userid, drugid, count):
    db.execute(
        'INSERT INTO dispense_history(timestamp, userid, drugid, count) VALUES (?, ?, ?, ?)',
        (timestamp, userid, drugid, count)
    )
    db.execute('UPDATE drug SET count = MAX(0, count - ?) WHERE drugid = ?', (count, drugid))

@in_transaction
def undo_dispense_history(db, timestamp, userid, drugid, count):
    curs = db.execute(
        'DELETE FROM dispense_history WHERE timestamp = ? AND userid = ? AND drugid = ? AND count = ?',
        (timestamp, userid, drugid, count)
    )
    if curs.rowcount == 0:
        return False

    db.execute('UPDATE drug SET count = MAX(0, count + ?) WHERE drugid = ?', (count, drugid))
    return True

DISPENSE_HISTORY_BASE = (
    Query().from_('dispense_history', 'dh')
    .add('dh', 'timestamp', 'userid', 'drugid', 'count')
    .join('drug', 'd', 'drugid = dh.drugid')
    .join('user', 'u', 'userid = dh.userid')
    .addas('u', 'name', 'user_name')
    .addas('d', 'name', 'drug_name')
    .add('d', 'strength')
    .order('timestamp', desc=True)
    .build())

def query_dispense_history(db, mintime=None, maxtime=None, userid=None, limit=None):
    query = DISPENSE_HISTORY_BASE.copy()
    args = {}

    if mintime is not None:
        query.where('timestamp >= $mintime')
        args['mintime'] = mintime

    if maxtime is not None:
        query.where('timestamp <= $maxtime')
        args['maxtime'] = maxtime

    if userid is not None:
        query.where('dh.userid = $userid')
        args['userid'] = userid

    if limit is not None:
        query.limit('$limit')
        args['limit'] = limit

    res = query.build().run(db, **args)
    for val in res:
        lt = time.localtime(val['timestamp'])
        val['date'] = time.strftime('%Y-%m-%d', lt)
        val['time'] = time.strftime('%H:%M:%S', lt)

    return res
