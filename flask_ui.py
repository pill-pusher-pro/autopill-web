from pathlib import Path

from flask_main import app
from flask_utils.utils import tojson
from utils import render_template

from flask import send_from_directory, request, abort

@app.route('/')
def ui_main():
    return render_template('tabui.html.jinja')

@app.route('/dispenser')
def ui_dispenser():
    return render_template('dispenser_ui.html.jinja')

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(Path(app.root_path) / 'static',
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')
