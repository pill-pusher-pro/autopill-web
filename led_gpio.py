import sys
import os
import time
import datetime
import logging
from pathlib import Path

from autopill_interface import AutopillInterface
from database import withdb, get_pending_schedules

log = logging.getLogger(__name__)

GPIO_BASE = Path('/sys/class/gpio')
GPIO_EXPORT = GPIO_BASE / 'export'
LEDS = {}
LED_INDEX = []
class LED:
    def __init__(self, color, pin, index):
        self.color = color
        self.pin = pin
        self.index = index
        self.fp = None
        self.fd = None
        self.value = None
        self.want_value = False
        self.path = GPIO_BASE / f'gpio{self.pin}' / 'value'

    def export(self):
        if not self.path.exists():
            try:
                with GPIO_EXPORT.open('w') as fp:
                    fp.write(f'{self.pin}\n')
            except IOError as e:
                log.warn(f'Cannot export GPIO {self.pin}: {e}')


    def _open(self):
        self.close()

        self.fp = self.path.open('wb')
        self.fd = self.fp.fileno()

        val = self.value
        if val is not None:
            self.value = None
            self.set(val)

    def close(self):
        if self.fp is not None:
            self.fp.close()
            self.fd = None
            self.fp = None

    def set(self, value):
        value = bool(value)
        if value != self.value:
            self.value = value
            if self.fd is not None:
                os.pwrite(self.fd, b'1\n' if value else b'0\n', 0)


def define_led(color, pin):
    led = LEDS[color] = LED(color, pin, len(LED_INDEX))
    LED_INDEX.append(led)
    return led

def open_all():
    log.info(f'Exporting all GPIO pins')
    for led in LED_INDEX:
        led.export()

    log.info(f'Opening LED pins...')
    tries = 8
    while True:
        all_opened = True
        for led in LED_INDEX:
            if not led.fp:
                try:
                    led._open()
                except IOError as e:
                    all_opened = False

        if all_opened:
            break

        tries -= 1
        if tries == 0:
            log.warn('Not able to open all LEDs')
            break

        time.sleep(.25)

    log.info(f'LED init complete')

def get_led(color):
    return LEDS[color]

def get_blink_state(db):
    for sched in get_pending_schedules(db, datetime.datetime.now()):
        idx = sched['led']
        if idx is not None:
            try:
                led = LED_INDEX[idx]
            except IndexError:
                continue

            led.want_value = True

@withdb
def blink_thread(db):
    intf = AutopillInterface.instance

    blink_state = False
    while True:
        try:
            for led in LED_INDEX:
                led.want_value = False

            blink_state = not blink_state
            if blink_state and intf.dispensing_result != 'INPROGRESS':
                get_blink_state(db)

            for led in LED_INDEX:
                led.set(led.want_value)

        except Exception:
            log.exception('Error in blink thread')
        time.sleep(1)
