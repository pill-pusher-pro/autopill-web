import sys
import os
import re
import time
import subprocess
import logging
from pathlib import Path

from flask_main import app
from flask_utils.utils import tojson, get_request_int
from flask import send_from_directory, request, abort

log = logging.getLogger(__name__)

@app.route('/api/system/reboot', methods=['POST'])
def api_reboot():
    subprocess.call(['sudo', 'reboot'])

@app.route('/api/system/shutdown', methods=['POST'])
def api_shutdown():
    subprocess.call(['sudo', 'poweroff'])

@app.route('/api/system/ipaddr')
def api_get_addr():
    res = subprocess.run(['ip', 'route', 'get', 'to', '1.1.1.1'], stdout=subprocess.PIPE, encoding='utf8')
    m = re.search(r'src (\S+)', res.stdout)
    if m:
        ip = m.group(1)
    else:
        ip = None
    return tojson({'ipaddr': ip})

@app.route('/api/system/update', methods=['POST'])
def api_update():
    from autopill_interface import AutopillInterface
    try:
        AutopillInterface.instance.update_firmware(Path(__file__).with_name('update-arduino.sh'))
    except Exception as e:
        log.error(f'Exception while updating firmware: {e}')
        return tojson({
            'result': False,
            'error': str(e)
        })
    return tojson({
        'result': True,
        'error': None
    })
