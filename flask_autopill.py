import time
import datetime
import logging

from flask_main import app
from autopill_interface import AutopillInterface
from flask_utils.utils import tojson, get_request_int, maintenance_task
from flask import send_from_directory, request, abort

import config

log = logging.getLogger(__name__)

def _wait_result(req, timeout, change_count):
    if timeout:
        if not req.wait(timeout, change_count):
            return tojson({'complete': False, 'result': None, 'message': 'timeout'})

    return tojson(req.tojson())

@maintenance_task
def clear_expired_requests():
    Autopill.instance.clear_expired_requests()

@app.route('/api/command')
def api_command():
    timeout = float(request.args.get('wait', '30'))
    prev_count = get_request_int('change_count')
    command = request.args.get('command')
    if not command:
        abort(400)

    intf = AutopillInterface.instance
    req = intf.command(command)
    return _wait_result(req, timeout, prev_count)

@app.route('/api/dispense')
def api_dispense():
    timeout = float(request.args.get('wait', '0'))
    prev_count = get_request_int('change_count')
    unit = get_request_int('unit')
    count = get_request_int('count')
    if count is None:
        count = 1
    intf = AutopillInterface.instance
    req = intf.queue_dispense(unit, count)
    return _wait_result(req, timeout, prev_count)

@app.route('/api/result/<int:id>')
def api_get_result(id):
    timeout = float(request.args.get('wait', '0'))
    prev_count = get_request_int('change_count')
    req = AutopillInterface.instance.get_request(id)
    if not req:
        abort(404)

    return _wait_result(req, timeout, prev_count)

@app.route('/api/config', methods=['GET', 'PUT'])
def api_config():
    intf = AutopillInterface.instance
    if request.method == 'PUT':
        data = request.get_json(force=True, cache=False)
        if 'motor_speed' in data:
            intf.set_motor_speed(data['motor_speed'])
        if 'timeout' in data:
            intf.set_dispense_timeout(data['timeout'])
        if 'info_level' in data:
            intf.set_info_level(data['info_level'])
        if 'motor_sequence' in data:
            intf.set_pillcheck(data['motor_sequence'])

        config.save_config()

    return tojson(config.CONFIG)

@app.route('/api/status')
def api_status():
    prev_count = get_request_int('count')
    timeout = get_request_int('timeout')

    intf = AutopillInterface.instance
    return tojson(intf.get_status(prev_count, timeout))


@app.route('/api/syncrtc', methods=['POST'])
def api_syncrtc():
    intf = AutopillInterface.instance
    now = datetime.datetime.utcnow()
    cmd = f'R {now.year} {now.month} {now.day} {now.hour} {now.minute} {now.second}'
    req = intf.command(cmd)
    result = req.tojson()
    result['cmd'] = cmd
    return tojson(result)
