'''
flask_main
==========

This module exists only to define the `Flask` app object.
Other modules will import this module and define their own routes.
'''

from flask import Flask
from flask_utils import run

app = Flask('flask_app', root_path=run.get_main_path(__name__))
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
