'use strict';

const wait = (amt) => new Promise(resolve => setTimeout(resolve, amt));

const notification_overlay = document.getElementById('notification-overlay');

let modal_showing = null;
let modal_stack = [];
let running_timeout = null;

let most_recent_failure_reasons = [];

const floatsearch = document.getElementById('float-search');

let RX_URL = /http[s]?:\/\/(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+/;

class ModalDialog {
    constructor(title) {
        this._on_exit = null;
        this._on_show = [];
        this._first_focus = null;
        this._on_validate = null;
        this.inputs = {};
        this.buttons = {};
        this.validators = [];

        this.valid = false;

        this.overlay = create_element('div', '', 'modal-overlay');
        this.dialog = create_element('form', '', '');
        this.fieldset = create_element('fieldset', '', 'modal-dialog');
        this.clientarea = create_element('div', '', 'modal-client-area');
        this.buttonarea = create_element('div', '', 'modal-button-area');
        this.dialog.appendChild(this.fieldset);
        this.legend = create_element('legend', title || '', '');
        if (title) {
            this.fieldset.appendChild(this.legend);
        }

        this.fieldset.appendChild(this.clientarea);
        this.fieldset.appendChild(this.buttonarea);

        this.overlay.appendChild(this.dialog);
    }

    setTitle(title) {
        this.legend.innerText = title;
    }

    onshow(f) {
        this._on_show.push(f);
        return this;
    }

    _do_show() {
        document.body.appendChild(this.overlay);

        this.overlay.addEventListener('click', evt => this.exit(0));
        this.overlay.addEventListener('keydown', (evt) => {
            evt.stopPropagation();
            if (evt.key == "Escape") {
                evt.preventDefault();
                this.exit('escape');
            }
            if (evt.key == "Enter" || evt.key == 'Return') {
                if (evt.target.tagName == 'TEXTAREA') {
                    return;
                }
                evt.preventDefault();
                this.exit('enter');
            }
        });

        this.dialog.addEventListener('submit', (evt) => {
            evt.stopPropagation(); evt.preventDefault(); this.exit('enter');
        });

        this.dialog.addEventListener('click', evt => evt.stopPropagation());

        modal_showing = this;
        if (this._first_focus) {
            this._first_focus.focus();
            if (this._first_focus.select) {
                this._first_focus.select();
            }
        }

        for (let func of this._on_show) {
            func(this, this.clientarea);
        }

        this.checkValidate();
    }

    show(onexit) {
        let rval = null;
        if (onexit) {
            this._on_exit = onexit;
        } else {
            rval = new Promise((resolve, reject) => { this._on_exit = resolve; });
        }

        if (modal_showing === null) {
            this._do_show();
        } else {
            modal_stack.push(this);
        }

        return rval;
    }

    validate(input, func) {
        if (func === undefined) {
            this.validators.push(input);
        } else {
            this.validators.push(dialog => {
                let elem = this.inputs[input];
                let res = func(elem.value, elem);
                if (!res)
                    elem.classList.add('validate-error');
                return res;
            });

        }
        return this;
    }

    onvalidate(func) {
        this._on_validate = func;
        return this;
    }

    checkValidate() {
        for (let input of Object.values(this.inputs)) {
            input.classList.remove('validate-error');
        }

        let valid = true;
        for (let func of this.validators) {
            let res = func();
            if (!res) {
                valid = false;
            }
        }
        this.valid = valid;
        if (this._on_validate) {
            this._on_validate(valid, this);
        }
        return valid;
    }

    button(text, value, func) {
        if (value === undefined)
            value = text.toLowerCase();

        let button = ce('button', {clas: `button-${value}`}, [text]);
        if (typeof(func) !== 'function') {
            func = evt => { evt.preventDefault(); this.exit(value); };
        }
        button.addEventListener('click', func);
        button.addEventListener('keydown', (evt) => {
            evt.stopPropagation();
            if (evt.key == "Escape") {
                this.exit('escape');
            }
        });

        this.buttonarea.appendChild(button);
        if (!this._first_focus) this._first_focus = button;

        this.buttons[value] = button;
        return this;
    }

    buttonspace() {
        this.buttonarea.appendChild(ce('div', {style: 'width: 100%'}));
        return this;
    }

    check(name, text, checked) {
        let label = create_element('label', '', '');
        let chk = create_element('input', '', '', {type:'checkbox', name});
        chk.checked = checked;
        this.inputs[name] = chk;
        label.appendChild(chk);

        append_text(label, text);
        this.buttonarea.appendChild(label);
        return this;
    }

    ok() { return this.button('OK'); }
    cancel() { return this.button('Cancel'); }
    yes() { return this.button('Yes'); }
    no() { return this.button('No'); }

    text(text) {
        this.clientarea.appendChild(create_element('div', text));
        return this;
    }

    elem(elem) {
        this.clientarea.appendChild(elem);
        return this;
    }

    label(text, elem) {
        if (text) {
            ce('div', {clas: 'modal-row'}, [
                ce('div', {clas: 'modal-label'}, [text]),
                elem
            ], this.clientarea);
        } else {
            this.clientarea.appendChild(elem);
        }
        return this;
    }

    addInput(name, elem) {
        this.inputs[name] = elem;
        elem.addEventListener('input', evt => {
            this.checkValidate();
        });
        if (!this._first_focus) this._first_focus = elem;
        return elem;
    }

    input(name, default_text, label) {
        let input = create_element('input', '', '', {type:'text', value: default_text || ''});
        this.addInput(name, input);
        this.label(label, input);
        return this;
    }

    textarea(name, rows, default_text, label) {
        let input = ce('textarea', {rows: rows, spellcheck: 'false'}, []);
        input.value = default_text || '';
        if (!this._first_focus) this._first_focus = input;
        this.inputs[name] = input;
        this.label(label, input);
        return this;
    }

    dropdown(name, value, options, label) {
        let select = ce('select', {}, []);
        if (!this._first_focus) this._first_focus = select;
        this.inputs[name] = select;
        for (let opt of options) {
            let option = create_element('option', opt[1], '', {value: opt[0]});
            if (value === opt[0]) {
                option.setAttribute('selected', '1');
            }
            select.appendChild(option);
        }
        this.label(label, select);
        return this;
    }

    exit(value) {
        if (modal_showing !== this) {
            return;
        }
        this.checkValidate();

        let exit = this._on_exit;
        if (exit) {
            try {
                let response = {value, inputs: this.inputs, buttons:this.buttons, dialog: this};
                if (exit(response) === false) {
                    return;
                }
            } catch (e) {
                console.error(e);
            }
        }
        this._on_exit = null;

        document.body.removeChild(this.overlay);
        modal_showing = null;

        if (modal_stack.length) {
            let next = modal_stack.pop();
            next._do_show();
        }
    }
}

function keyName(evt) {
    let mods = '';
    if (evt.ctrlKey)
        mods += 'ctrl-';
    if (evt.altKey)
        mods += 'alt-';
    if (evt.shiftKey)
        mods += 'shift-';

    return mods + evt.key.toLowerCase();
}

class Tab {
    constructor(title, id) {
        this.title = title;
        this.id = id;
        this.active = false;

        this.keybind_press = {};
        this.keybind_down = {};
        this.keybind_up = {};

        let button = document.createElement('button');
        this.tabButton = button;
        button.innerText = title;
        button.addEventListener('click', evt => selectTab(this));
    }

    bind(key, press, release) {
        if (press)
            this.keybind_down[key] = press;
        if (release)
            this.keybind_up[key] = release;
    }

    createTabElem() {
        let parent = document.getElementById('tab-content');
        this.tabElem = ce('div', {clas: 'tab'}, [], parent);
    }

    update() {

    }

    onActivate() {
    }

    onSelect() {
    }

    onDeactivate() {
    }

    onKey(evt) {
    }

    checkAutoUpdate() {
    }

    onResize() {
    }
}

function add_notification(text) {
    let div = create_element('div', text, 'notification-box');
    function remove() {
        notification_overlay.removeChild(div);
    }
    div.addEventListener('click', remove);
    setTimeout(remove, 3000);
    notification_overlay.appendChild(div);
}

function add_magic(elem, text, patterns, func) {
    let regex = new RegExp(`^(.*?)(${patterns})(.*)$`, 's');
    while (true) {
        let m = regex.exec(text);
        if (!m) {
            break;
        }
        append_text(elem, m[1]);
        elem.appendChild(func(m[2]));
        text = m[3];
    }
    append_text(elem, text);
}

function convert_urls_to_links(elem, text) {
    add_magic(elem, text, RX_URL.source,
              url => create_element(
                  'a', url, '', {
                      href: url,
                      target: '_blank'
                  }));
    return elem;
}

async function fetchJSON(url, method, body) {
    let options = {method: method};
    if (body) {
        options.body = JSON.stringify(body);
    }
    let resp = await fetch(url, options);
    if (!resp.ok) {
        throw new Error(`HTTP error ${resp.status}: ${resp.statusText}`);
    }
    let text = await resp.text();
    return JSON.parse(text);
}

async function fetchJSONmb(url, method, body) {
    try {
        return await fetchJSON(url, method, body);
    } catch(e) {
        await new ModalDialog('Error').ok().text(`${e}`).show();
        throw(e);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
// DOM utilities
//////////////////////////////////////////////////////////////////////////////////////////

function append_text(elem, text) {
    elem.appendChild(document.createTextNode(text));
}


// Create element. `attrs` is mapping of attributes (with 'clas' mapping to 'class' to avoid keyword clash)
function ce(tag, attrs, children, parent) {
    let elem = document.createElement(tag);
    if (attrs) {
        if (attrs.clas) {
            elem.setAttribute('class', attrs.clas);
        }
        for (let [key, val] of Object.entries(attrs)) {
            if (key != 'clas') {
                elem.setAttribute(key, val);
            }
        }
    }
    if (children) {
        for (let child of children) {
            if (typeof(child) === 'string' || typeof(child) == 'number') {
                append_text(elem, child);
            } else {
                elem.appendChild(child);
            }
        }
    }
    if (parent) {
        parent.appendChild(elem);
    }
    return elem;
}

// create_element(tag, text, clas, attrs) OR
// create_element(tag, clas, attrs, children)
function create_element(tag, text, clas, attrs) {
    let elem = null;
    if (Array.isArray(attrs)) {
        elem = ce(tag, clas, attrs);
        clas = text;
    } else {
        elem = ce(tag, attrs, text ? [text] : []);
    }
    if (clas) {
        elem.setAttribute('class', clas);
    }
    return elem;
}

function create_link(href, text, clas, attrs) {
    let link = create_element('a', text, clas, attrs);
    link.setAttribute('href', href);
    return link;
}
function create_poplink(href, text, clas, attrs) {
    let link = create_link(href, text, clas, attrs);
    link.setAttribute('target', '_blank');
    return link;
}

function getElem(elem) {
    if (typeof(elem) === 'string') {
        return document.querySelector(elem);
    }
    return elem;
}

function bindEvent(selector, event, func) {
    if (typeof(selector) === 'string') {
        for (let elem of document.querySelectorAll(selector)) {
            elem.addEventListener(event, func);
        }
    } else {
        if (selector) {
            selector.addEventListener(event, func);
        }
    }
}

function bindClick(elem, func) {
    bindEvent(elem, 'click', func);
}

// Returns:
//   -1 if element is partially or completely scrolled off the top
//    0 if element is completely visible
//    1 if element is partially or completely scrolled off the bottom
function scroll_position(elem, relative_to) {
    let elemrect = elem.getBoundingClientRect();
    let parentrect;
    if (relative_to === window) {
        parentrect = new DOMRect(0, 0, window.innerWidth, window.innerHeight);
    } else {
        parentrect = relative_to.getBoundingClientRect();
    }

    if (elemrect.top < parentrect.top) {
        return -1;
    } else if (elemrect.bottom > parentrect.bottom) {
        return 1;
    } else {
        return 0;
    }
}

function scrollIfNeeded(elem, relative_to) {
    switch(scroll_position(elem, relative_to)) {
    case -1:
        elem.scrollIntoView(true);
        break;
    case 1:
        elem.scrollIntoView(false);
        break;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
// Text utilities
//////////////////////////////////////////////////////////////////////////////////////////

function duration_text(ms) {
    // If this is the case, it's probably a time sync issue between the browser and
    // the runner.

    if (ms < 0)
        ms = 0;
    let seconds = Math.floor(ms / 1000);
    let minutes = Math.floor(seconds / 60);
    let hours = Math.floor(minutes / 60);
    ms %= 1000;
    seconds %= 60;
    minutes %= 60;
    let text = rjust(''+seconds, 2, '0');
    if (hours) {
        text = `${hours}:${rjust(''+minutes, 2, '0')}:${text}`;
    } else {
        text = `${minutes}:${text}`;
    }
    return text;
}

// Behaves like Python str.rjust
function rjust(str, len, char) {
    let numpad = len - str.length;
    if (numpad > 0) {
        return char.repeat(numpad) + str;
    }
    return str;
}

function iso_date(dt) {
    let y = rjust(`${dt.getFullYear()}`, 4, '0');
    let m = rjust(`${dt.getMonth() + 1}`, 2, '0');
    let d = rjust(`${dt.getDate()}`, 2, '0');
    return `${y}-${m}-${d}`;
}

function iso_time(dt) {
    let h = rjust(`${dt.getHours()}`, 2, '0');
    let m = rjust(`${dt.getMinutes()}`, 2, '0');
    let s = rjust(`${dt.getSeconds()}`, 2, '0');
    return `${h}:${m}:${s}`;
}

function short_date(dt) {
    let m = rjust(`${dt.getMonth() + 1}`, 2, '0');
    let d = rjust(`${dt.getDate()}`, 2, '0');
    return `${m}/${d}`;
}

function short_time(dt) {
    let h = rjust(`${dt.getHours()}`, 2, '0');
    let m = rjust(`${dt.getMinutes()}`, 2, '0');
    return `${h}:${m}`;
}

function iso_timestamp(dt) {
    return `${iso_date(dt)} ${iso_time(dt)}`;
}

function short_timestamp(dt) {
    return `${short_date(dt)} ${short_time(dt)}`;
}

function dash(num) {
    return num ? `${num}` : '-';
}

function capitalize(text) {
    return text.substr(0, 1).toUpperCase() + text.substr(1).toLowerCase();
}

function build_query(params) {
    return Object.entries(params).map(v => `${v[0]}=${encodeURIComponent(v[1])}`).join('&');
}

function setclass(elem, clas, val) {
    let contains = elem.classList.contains(clas);
    if (!!val != !!contains) {
        if (val) {
            elem.classList.add(clas);
        } else {
            elem.classList.remove(clas);
        }
    }
}

function shallow_copy_obj(source) {
    let new_obj = {};
    for (let [key, val] of Object.entries(source)) {
        new_obj[key] = val;
    }
    return new_obj;
}

function compare_obj(a, b) {
    if (a === b) return true;
    if (typeof(a) === 'object') {
        if (Array.isArray(a)) {
            if (!Array.isArray(b)) return false;
            if (a.length != b.length) return false;
            for (let i = 0; i < a.length; i++) {
                if (!compare_obj(a[i], b[i])) return false;
            }
        } else {
            let ka = Object.keys(a);
            let kb = Object.keys(b);
            if (ka.length != kb.length) return false;
            for (let key of ka) {
                if (!compare_obj(a[key], b[key])) return false;
            }
        }
        return true;
    }
    return false;
}

if (floatsearch) {
    let container = floatsearch.parentElement;
    let update = evt => {
        let float = scroll_position(container, window) == -1;
        setclass(floatsearch, 'floating', float);
        if (!float) {
            container.style.height = `${floatsearch.clientHeight}px`;
        }
    };
    window.addEventListener('scroll', update);
    update();
}
