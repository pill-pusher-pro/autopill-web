
let afterUnitSelect = () => null;

let diagCommands = [
    ['t', 'Diag info', ['t']],
    ['p', 'Dispense 1', ['p', ' ']],
    ['p2', 'Dispense 2'],
    ['p100', 'Dispense 100', ['alt-p']],
    ['c', 'Cancel Dispense', 'c'],
    ['v0', 'Servo in', '/'],
    ['v30', 'Servo neutral', '.'],
    ['v90', 'Servo out', ','],
    ['v140', 'Servo release', 'm'],
];

async function sendCommand(cmd) {
    let res = await fetchJSON(`/api/command?command=${encodeURIComponent(cmd)}`);
    return res;
}

function setDiagResultText(text) {
    for (let elem of document.getElementsByClassName('diag-result')) {
        elem.innerText = text;
    }
}

async function diagCommand(cmd) {
    setDiagResultText('...');
    let resp = await sendCommand(cmd);
    let text = resp.result ? '' : '<error> ';
    text += resp.message;
    if (resp.info)
        for (let line of resp.info) {
            text += '\n' + line;
        }

    setDiagResultText(text);

    return resp;
}


class DiagTab extends Tab {
    constructor(title, id) {
        super(title, id);
        this.tabElem = document.getElementById('diagtab');

        let tbl = document.getElementById('diagcommands');
        tbl.innerHTML = '';
        for (let cmd of diagCommands) {
            this.addCommand(tbl, cmd[0], cmd[1], cmd[2]);
        }

        let fs = document.getElementById('unit-select');
        let btn = ce('button', {clas: 'unit-button'}, [`Off`], fs);
        bindClick(btn, evt => {
            evt.preventDefault();
            diagCommand(`U`);
            afterUnitSelect();
        });
        for (let i = 0; i < 10; i++) {
            let btn = ce('button', {clas: 'unit-button'}, [`${i}`], fs);
            bindClick(btn, evt => {
                evt.preventDefault();
                diagCommand(`u${i}`);
                afterUnitSelect();
            });
            if (i < 10) {
                this.bind(`ctrl-${i}`, () => diagCommand(`u${i}`));
            }
        }
        this.bindMotor('h', 'j', 'a');
        this.bindMotor('k', 'l', 'b');

        for (let i = 0; i < 3; i++) {
            this.addDispenseRow(i);
        }

        document.getElementById('add-dispense-command').addEventListener('click', evt => {
            evt.preventDefault();
            this.addDispenseRow(0);
        });

        document.getElementById('start-dispense-command').addEventListener('click', evt => {
            evt.preventDefault();
            this.startDispense();
        });

        this.getStatus();

    }

    async startDispense() {
        let tbl = document.querySelector('#dispense-commands tbody');
        for (let row of tbl.getElementsByTagName('tr')) {
            let unit = row.querySelector('input[name="unit"]');
            let count = row.querySelector('input[name="count"]');
            unit = parseInt(unit.value);
            count = parseInt(count.value);
            if (count) {
                let resp = await fetchJSON(`/api/dispense?unit=${unit}&count=${count}`);
                console.log(resp);
            }

        }
    }

    addDispenseRow(unit) {
        let tbl = document.getElementById('dispense-commands');
        let body = tbl.querySelector('tbody');
        let tr = ce('tr', {}, [], body);

        let td = ce('td', {}, [], tr);
        let rmbutton = ce('button', {}, ['Del'], td);
        rmbutton.addEventListener('click', evt => {
            evt.preventDefault();
            tr.parentElement.removeChild(tr);
        });

        td = ce('td', {}, [], tr);
        ce('input', {type: 'number', min: '0', max: '15', name: 'unit', value: `${unit}`}, [], td);


        td = ce('td', {}, [], tr);
        ce('input', {type: 'number', min: '0', max: '100', name: 'count', value: '0'}, [], td);

    }

    bindMotor(keyb, keyf, which) {
        let fpress = false;
        let bpress = false;
        let lcmd = '0';
        function update() {
            let cmd = '0';
            if (fpress && !bpress) cmd = 'f';
            if (bpress && !fpress) cmd = 'b';
            if (cmd != lcmd) {
                lcmd = cmd;
                diagCommand(`${which}${cmd}`);
            }
        }
        let elem = document.getElementById(`motor-${which}-b`);
        bindEvent(elem, 'mousedown', evt => { if (evt.button == 0) { bpress = true; update(); }})
        bindEvent(elem, 'mouseup', evt => { if (evt.button == 0) { bpress = false; update(); }})

        elem = document.getElementById(`motor-${which}-f`);
        bindEvent(elem, 'mousedown', evt => { if (evt.button == 0) { fpress = true; update(); }})
        bindEvent(elem, 'mouseup', evt => { if (evt.button == 0) { fpress = false; update(); }})


        this.bind(keyb, () => { bpress = true; update(); }, () => {bpress = false; update(); } );
        this.bind(keyf, () => { fpress = true; update(); }, () => {fpress = false; update(); } );
    }

    addCommand(tbl, cmd, label, keys) {
        let btn = ce('button', {}, [label], tbl);
        btn.addEventListener('click', evt => {
            evt.preventDefault();
            diagCommand(cmd);
        });
        if (keys) {
            for (let key of keys) {
                this.keybind_down[key] = () => diagCommand(cmd);
            }
        }
    }

    async getStatus() {
        let prev_count = -1;
        while (true) {
            try {
                let data = await fetchJSON(`/api/status?count=${prev_count}&timeout=30`);
                prev_count = data.status_count;
                document.getElementById('dispense-state').innerText = data.state;
                document.getElementById('dispense-result').innerText = data.result;
                document.getElementById('dispense-count').innerText = data.count;
                document.getElementById('dispense-remain').innerText = data.remain;
                document.getElementById('pill-in-chamber').innerText = data.pill_in_chamber;
            } catch (e) {
                console.error(e);
                await wait(1000);
            }
        }

    }

    refresh_info() {
    }

    onSelect() {

    }

    onKey(evt) {
    }

    update() {
        this.refresh_info();
    }
};
