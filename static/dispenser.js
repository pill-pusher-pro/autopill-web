'use strict';

const SHUTDOWN_TEXT = 'Press Continue to shut down. When screen turns white, it is safe to remove power.';

let dispenseInProgress = false;
let schedulerId = 0;
let scheduleTimeout = null;

let headerDiv = document.getElementById('header');
let dispenserDiv = document.getElementById('dispenser');
let footerButton = document.getElementById('footer-button');

let scrollBack = document.getElementById('scroll-back');
let scrollFwd = document.getElementById('scroll-fwd');

let pendingByUser = {};
let pendingByUserList = [];

let rootPage;
let navStack = [];
let currentPage = null;
let uiPageById = {};

//let uiPages = Array.prototype.slice.call(document.getElementsByClassName('uipage'));

class UIPage {
    get navText() { return 'Back' }

    constructor(elemId, title) {
        this.args = {};
        this.elemId = elemId;
        this.title = title;
        if (this.elemId) {
            this.elem = document.getElementById(this.elemId);
            this.elem.style.display = 'none';
            this.title = this.elem.getAttribute('data-title') || '\u00a0';
            bindEvent(this.elem, 'scroll', evt => {
                this.checkScroll();
            })
        }
    }

    onActivate(returning) {
    }

    onDeactivate() {

    }

    onFooterNav() {
        uiNavBack();
    }

    addButton(lbl, func) {
        let btn = ce('button', {clas: 'nav-button'}, [lbl], this.elem);
        bindClick(btn, func);
        return btn;
    }

    checkScroll() {
        let elem = this.elem;
        scrollBack.style.visibility = elem.scrollTop > 0 ? 'visible' : 'hidden';
        scrollFwd.style.visibility = Math.abs(elem.scrollHeight - elem.clientHeight - elem.scrollTop) >= 1 ? 'visible' : 'hidden';
    }
};

function defPage(elemId, clas) {
    let obj = new clas(elemId);
    uiPageById[elemId] = obj;
    return obj;
}

for (let elem of document.getElementsByClassName('uipage')) {
    let id = elem.getAttribute('id');
    if (id)
        defPage(id, UIPage);
}

rootPage = currentPage = defPage('dispenser', class extends UIPage {
    get navText() { return 'Menu' }

    onFooterNav() {
        uiNav('menu');
    }
});

function sorter(f) {
    return (a, b) => {
        let keyA = f(a);
        let keyB = f(b);
        if (Array.isArray(keyA)) {
            for (let i = 0; i < keyA.length; i++) {
                let va = keyA[i];
                let vb = keyB[i];
                if (va < vb) return -1;
                if (va > vb) return 1;
            }
            return 0;
        } else {
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        }
    };
}

const sortDrug = sorter(d => [d.active ? 0 : 1, d.hopper_index == null ? 1 : -d.hopper_index]);

defPage('drugs', class extends UIPage {
    async onActivate() {
        this.elem.innerHTML = '';
        //let status = ce('div', {clas: 'manual-dispense-status'}, ['\u00a0'], elem);
        let resp = await fetchJSON('/api/drug?include_inactive=1');
        let drugs = resp.results; //.filter(d => d.active && d.hopper_index !== null);
        drugs.sort(sortDrug);
        for (let drug of drugs) {
            let btn = this.addButton(drug.name, async evt => {
                uiNav('drug-menu', {drug});
            });
            if (!drug.active) btn.classList.add('drug-inactive');
        }
        this.checkScroll();
    }
});

defPage('drug-menu', class extends UIPage {
    onActivate() {
        let drug = this.args.drug;
        this.elem.innerHTML = '';
        this.drug = drug;
        setTitle(`Drug: ${this.drug.name}`);

        let status = ce('div', {clas: 'manual-dispense-status'}, ['\u00a0'], this.elem);

        if (drug.hopper_index) {
            fetchJSON(`/api/command?command=u${drug.hopper_index}`);
        }
        //

        drug.id = drug.drugid;
        let active = activeCheckbox('drug')(null, drug)[0];
        ce('label', {}, [active, 'Active'], this.elem);

        this.addButton('Dispense', async evt => {
            status.innerText = 'Dispensing...';
            try {
                let resp = await fetchJSON(`/api/drug/${drug.id}/dispense?count=1`, 'POST', {});
                let req = resp.request;
                drug.count = resp.drug.count;
                countDisplay.innerText = `${drug.count}`;

                resp = await fetchJSON(`/api/result/${req.id}?wait=30`);
                drug.count -= resp.pill_count;
                countDisplay.innerText = `${drug.count}`;

                status.innerText = resp.message;
            } catch (e) {
                status.innerText = `${e}`;
            }
        });
        this.addButton('Count', async evt => {
            let resp = await countDrug(this.drug);
            if (resp.result) {
                countDisplay.innerText = `${resp.count}`;
            }
        });

        let countDiv = ce('div', {clas: 'manage-drug-count'}, [], this.elem);
        let countDisplay = ce('div', {clas: 'drug-count'}, [`${drug.count}`]);

        function countBtn(amt) {
            let btn = ce('button', {clas: 'drug-count-button'}, [amt], countDiv);
            amt = parseInt(amt);
            bindClick(btn, async evt => {
                let resp = await fetchJSONmb(`/api/drug/${drug.id}/adjust`, 'POST', {amount: amt});
                countDisplay.innerText = `${resp.count}`;
            });
        }
        countBtn('+1');
        countBtn('+10');
        countBtn('+30');
        countDiv.appendChild(countDisplay);
        countBtn('-1');
        countBtn('-10');
        countBtn('-30');

    }
    onDeactivate() {
        if (this.drug.hopper_index) {
            fetchJSON(`/api/command?command=U`);
        }
    }
});

const sortUser = sorter(u => [u.active ? 0 : 1, u.id]);

defPage('users', class extends UIPage {
    async onActivate() {
        this.elem.innerHTML = '';
        let resp = await fetchJSON('/api/user?include_inactive=1');
        let users = resp.results;
        users.sort(sortUser);
        for (let user of users) {
            let btn = this.addButton(user.name, async evt => {
                uiNav('user-menu', {user});
            });
            if (!user.active) btn.classList.add('user-inactive');
        }
        this.checkScroll();
    }
});

const sortDose = sorter(d => [d.active ? 0 : 1, d.time]);

defPage('user-menu', class extends UIPage {
    async onActivate(args) {
        let user = this.args.user;
        this.elem.innerHTML = '';
        this.user = user;
        setTitle(`User: ${this.user.name}`);
        user.id = user.userid;

        let active = activeCheckbox('user')(null, user)[0];
        ce('label', {}, [active, 'Active'], this.elem);

        let resp = await fetchJSONmb(`/api/schedule?full=1&userid=${user.id}&include_inactive=1`);
        resp.results.sort(sortDose);

        for (let dose of resp.results) {
            dose.id = dose.scheduleid;
            let div = ce('div', {clas: 'dose-schedule-entry'}, [], this.elem);
            ce('span', {clas: 'dose-schedule-drug'}, [dose.drug_name], div);
            ce('span', {clas: 'dose-schedule-time'}, [dose.time_text], div);
            ce('span', {clas: 'dose-schedule-count'}, [dose.count], div);
            ce('span', {clas: 'dose-schedule-filter'}, [dose.date_filter], div);
            ce('span', {clas: 'dose-schedule-last-dispense'}, [dose.last_dispense_iso], div);
            let active = activeCheckbox('schedule')(null, dose)[0];
            ce('label', {clas: 'dose-schedule-active'}, [active, 'Active'], div);

            let btn = ce('button', {clas: 'dose-button'}, ['...'], div);
            bindClick(btn, async evt => {
                uiNav('dose-menu', {dose});
            });
            //btn = ce('button', {clas: 'clear-button'}, ['Clear'], div);
        }

        this.checkScroll();
    }
});

defPage('dose-menu', class extends UIPage {
    async onActivate() {
        let dose = this.args.dose;
        this.elem.innerHTML = '';
        this.dose = dose;
        setTitle(`${this.dose.user_name} / ${this.dose.drug_name}`);

        let active = activeCheckbox('schedule')(null, dose)[0];
        ce('label', {}, [active, 'Active'], this.elem);

        this.addButton('Dispense', async () => {
            uiNav('dispense-dose', {schedList: [dose]});
        });
        this.addButton('Reset', async () => {
            let resp = await new ModalDialog('Reset Dose').text('Are you sure you want to reset this dose?').yes().no().show();
            if (resp.value === 'yes') {
                await fetchJSON(`/api/schedule/${dose.id}/reset`, 'POST', {});
                cancelPendingCheck();
                startScheduleCheck();
                uiNavBack();
            }
        });
        this.addButton('Clear pending', async () => {
            let resp = await new ModalDialog('Clear Pending Dose').text('Are you sure you want to clear this dose?').yes().no().show();
            if (resp.value === 'yes') {
                await fetchJSON(`/api/schedule/${dose.id}/clear`, 'POST', {});
                cancelPendingCheck();
                startScheduleCheck();
                uiNavBack();
            }
        });
    }
});

defPage('history', class extends UIPage {
    async onActivate(args) {
        this.elem.innerHTML = '';
        let resp = await fetchJSONmb(`/api/history?limit=50`);
        for (let entry of resp.results) {
            let div = ce('div', {clas: 'history-entry'}, [], this.elem);
            ce('span', {clas: 'history-date'}, [entry.date], div);
            ce('span', {clas: 'history-time'}, [entry.time], div);
            ce('span', {clas: 'history-user'}, [entry.user_name], div);
            ce('span', {clas: 'history-drug'}, [entry.drug_name], div);
            ce('span', {clas: 'history-count'}, [entry.count], div);
            let btn = ce('button', {clas: 'undo-button'}, ['Undo'], div);
            bindClick(btn, async evt => {
                let drug = await fetchJSONmb(`/api/drug/${entry.drugid}`);
                let unit = drug.hopper_index;
                let text;
                if (unit) {
                    await fetchJSONmb(`/api/command?command=u${unit}`);
                    text = `Please replace ${entry.count} of ${entry.drug_name} into hopper ${unit}, then click OK.`;
                } else {
                    text = `Drug ${entry.drug_name} does not have an assigned hopper, continue?`;
                }
                let resp = await new ModalDialog('Undo Dispense').ok().cancel().text(text).show();
                try {
                    if (resp.value === 'enter' || resp.value === 'ok') {
                        let resp = await fetchJSONmb('/api/history/undo', 'POST', entry);
                        div.parentElement.removeChild(div);
                    }
                } finally {
                    if (unit) {
                        await fetchJSONmb(`/api/command?command=U`);
                    }
                }
            })
        }
        this.checkScroll();
    }
});

defPage('ipaddr', class extends UIPage {
    async onActivate() {
        this.elem.innerHTML = '';
        let res = await fetchJSON('/api/system/ipaddr');
        ce('div', {}, [`IP Address: ${res.ipaddr}`], this.elem);

    }
});

rootPage.onActivate();
rootPage.checkScroll();
rootPage.elem.style.removeProperty('display');


function setTitle(title) {
    headerDiv.innerText = title;
}

function _uiNav(page, returning, args) {
    console.log(page.elemId, returning)
    currentPage.onDeactivate();
    currentPage.elem.style.display = 'none';
    currentPage = page;
    setTitle(page.title);
    footerButton.innerText = page.navText;
    currentPage.elem.style.removeProperty('display');
    currentPage.args = args;
    currentPage.onActivate(returning);
    currentPage.checkScroll();
}

function uiNav(page, args) {
    let newPage = uiPageById[page];
    navStack.push([currentPage, currentPage.args]);
    _uiNav(newPage, false, args || {});
}

function uiNavBack() {
    if (navStack.length) {
        let [page, args] = navStack.pop();
        _uiNav(page, true, args);
    }
}
function uiNavRoot() {
    navStack = [];
    _uiNav(rootPage, true);
}

function scheduleNextCheck() {
    if (scheduleTimeout) {
        clearTimeout(scheduleTimeout);
    }
    let sid = schedulerId;
    let ctime = new Date().getTime();
    let next_minute = 60000 * Math.ceil((ctime + 5000) / 60000);
    scheduleTimeout = setTimeout(() => checkPendingSchedules(sid), next_minute - ctime);
}

async function checkPendingSchedules(sid) {
    try {
        if (schedulerId !== sid) return;
        let resp = await fetchJSON('/api/uimain');
        if (schedulerId !== sid) return;

        dispenserDiv.innerHTML = '';

        let clock = ce('div', {clas: 'clock'}, [], dispenserDiv);
        let clockTime = ce('div', {clas: 'clock-time'}, [], clock);
        let clockDate = ce('div', {clas: 'clock-date'}, [], clock);
        let date = new Date();
        clockDate.innerText = date.toLocaleDateString('en-US', {year:'numeric', month:'short', weekday:'short', day:'numeric'})
        clockTime.innerText = date.toLocaleTimeString('en-US', {hour:'numeric', minute:'numeric'});


        pendingByUser = {};
        pendingByUserList = [];
        for (let sched of resp.pending) {
            let userList = pendingByUser[sched.userid];
            if (!userList) {
                userList = pendingByUser[sched.userid] = [];
                pendingByUserList.push([sched.userid, userList]);
            }
            userList.push(sched);
        }
        pendingByUserList.sort((a, b) => a[0] - b[0]);
        for (let [userid, userList] of pendingByUserList) {
            let db = ce('button', {clas: 'dispense-button'}, [], dispenserDiv);
            ce('div', {clas: 'dispense-user'}, [userList[0].user_name], db);
            let schedDiv = ce('div', {clas: 'dispense-scheds'}, [], db);
            for (let sched of userList) {
                let div = ce('div', {clas: 'dispense-sched'}, [], schedDiv);
                fillScheduleDiv(div, sched);
            }
            db.addEventListener('click', () => askDispense(userList));
        }

        if (resp.reorder.length) {
            ce('div', {clas: 'dispense-header'}, ['Running low:'], dispenserDiv);
            let tbl = ce('table', {clas: 'dispense-reorder'}, [], dispenserDiv);
            ce('thead', {}, [
                ce('tr', {}, [
                    ce('th', {}, ['Drug']),
                    ce('th', {}, ['Count']),
                ])
            ], tbl);
            let tbody = ce('tbody', {}, [], tbl);
            for (let drug of resp.reorder) {
                ce('tr', {}, [
                    ce('td', {clas: 'dispense-reorder-drug'}, [drug.name]),
                    ce('td', {clas: 'dispense-reorder-count'}, [`${drug.count}`])
                ], tbody);
            }
        } else if (pendingByUserList.length == 0) {
            ce('div', {clas: 'dispense-header'}, ['No medications due'], dispenserDiv);
        }



        ce('div', {clas: 'spacer'}, ['\u00a0'], dispenserDiv);
        let reload = ce('button', {clas: 'reload'}, ['Refresh'], dispenserDiv);
        bindClick(reload, () => { location.reload(); });

        scheduleNextCheck();
        currentPage.checkScroll();
    } catch(e) {
        console.error(e);
        setTimeout(() => checkPendingSchedules(sid), 2000);
    }
}

function fillScheduleDiv(div, sched) {
    ce('span', {clas: 'dispense-time'}, [sched.time_text], div);
    ce('span', {clas: 'dispense-count'}, [sched.count], div);
    ce('span', {clas: 'dispense-drug'}, [sched.drug_name], div);
}

async function cancelDispense() {
    let res = await fetchJSON(`/api/command?command=c`);
}

defPage('dispense-dose', class extends UIPage {
    get navText() { return 'Cancel' }

    async doDispense(schedList) {
        //let hdr = ce('div', {clas: 'dispense-header'}, ['Dispensing'], this.elem);
        let schedDiv = ce('div', {clas: 'dispense-sched'}, [], this.elem);
        //let cb = ce('button', {clas: 'cancel-dispense'}, ['Cancel'], this.elem);
        //cb.addEventListener('click', () => cancelDispense());

        for (let sched of schedList) {
            schedDiv.innerHTML = '';
            fillScheduleDiv(schedDiv, sched);
            this.active = true;
            let req = await fetchJSON(`/api/schedule/${sched.scheduleid}/dispense`, 'POST', {});
            let reqId = req.request.id;
            let resp = await fetchJSON(`/api/result/${reqId}?wait=120`);
            this.active = false;
            if (!resp.result) {
                if (resp.message == 'CANCELLED') {
                    setTitle('Dispense Cancelled');
                    schedDiv.parentElement.removeChild(schedDiv);
                    //cb.parentElement.removeChild(cb);
                    await wait(2000);
                    return;
                }
                let message = resp.message;
                if (message == 'FAIL_EMPTY') {
                    message = `Hopper ${sched.hopper_index} is empty`;
                } else if (message == 'FAIL_STUCK') {
                    message = `Hopper ${sched.hopper_index} has a stuck pill`;
                }

                let response = await new ModalDialog('Error').text(`${message}`).button('Continue').button('Cancel').show();
                if (response.value !== 'continue') {
                    return;
                }
            }
        }
        this.active = false;
        setTitle('Dispense Complete');
        schedDiv.parentElement.removeChild(schedDiv);
        //cb.parentElement.removeChild(cb);
        await wait(2000);
    }

    async onActivate() {
        let schedList = this.args.schedList;
        cancelPendingCheck();

        this.elem.innerHTML = '';
        setTitle('Dispensing');

        try {
            await this.doDispense(schedList);
        } finally {
            uiNavBack();
            cancelPendingCheck();
            startScheduleCheck();
        }
    }

    onDeactivate() {
        if (this.active) {
            cancelDispense();
        }
    }
});

async function askDispense(schedList) {
    try {
        let dialog = new ModalDialog(`Dispense drugs for ${schedList[0].user_name}`).button('Clear').buttonspace().ok().cancel();
        //let schedDiv = ce('div', {clas: 'dispense-scheds'}, []);
        //dialog.elem(schedDiv);

        for (let sched of schedList) {
            let active = ce('input', {type: 'checkbox', checked: '1'}, []);
            let row = ce('label', {clas: 'dispense-sched'}, [active]);
            //let div = ce('span', {}, [], row);
            sched.activeCheck = active;
            fillScheduleDiv(row, sched);
            dialog.elem(row);

        }


        let resp = await dialog.show();

        schedList = schedList.filter(d => d.activeCheck.checked);
        if (schedList.length == 0) {
            return;
        }

        if (resp.value == 'ok' || resp.value == 'enter') {
            uiNav('dispense-dose', {schedList});
        } else if (resp.value == 'clear') {
            for (let sched of schedList) {
                await fetchJSON(`/api/schedule/${sched.scheduleid}/clear`, 'POST', {});
            }
        }

    } catch(e) {
        await new ModalDialog('Error').text(`${e}`).ok().show();
        console.error(e);
    } finally {
        cancelPendingCheck();
        startScheduleCheck();
    }
}

function startScheduleCheck() {
    //new ModalDialog('test').text('test').ok().show();
    checkPendingSchedules(schedulerId);
}

function cancelPendingCheck() {
    if (scheduleTimeout) {
        clearTimeout(scheduleTimeout);
        scheduleTimeout = null;
    }
    schedulerId++;
}

function countDrug(drug) {
    let done = false;
    let baseCount = 0;
    let count = 0;
    let dispenseRequest = null;
    let mainText = ce('span', {}, []);
    let dialog = new ModalDialog('Count drug').elem(mainText);
    mainText.innerText = `Ready to count ${drug.name}. Make sure pill tray is present and empty.`;
    dialog.button('Start').button('Stop').button('Save').button('Cancel');
    dialog.buttons.save.disabled = true;
    dialog.buttons.stop.style.display = 'none';

    async function doCountDrug() {
        mainText.innerText = `Counting ${drug.name}: ${count}`;
        while (1) {
            if (done) return;
            if (dispenseRequest == null) {
                baseCount = count;
                dispenseRequest = await fetchJSON(`/api/dispense?unit=${drug.hopper_index}&count=1000&change_count=0&wait=30`);
            } else {
                dispenseRequest = await fetchJSON(`/api/result/${dispenseRequest.id}?&change_count=${dispenseRequest.change_count}&wait=30`);
            }

            count = baseCount + dispenseRequest.pill_count;

            if (dispenseRequest.result === null) {
                mainText.innerText = `Counting ${drug.name}: ${count}`;
            } else {
                dispenseRequest = null;
                mainText.innerText = `Count for ${drug.name}: ${count}`;
                dialog.buttons.start.disabled = false;
                dialog.buttons.save.style.removeProperty('display');
                dialog.buttons.stop.style.display = 'none';
                dialog.buttons.start.innerText = 'Continue';
                dialog.buttons.save.disabled = false;
                return;
            }
        }

    }

    let resolveFunc;

    dialog.show(resp => {
        if (resp.value === 'save') {
            fetchJSONmb(`/api/drug/${drug.id}`, 'PUT', {count: count});
        } else if (resp.value === 'stop') {
            fetchJSON('/api/command?command=c');
            return false;
        } else if (resp.value === 'start' || resp.value === 'enter') {
            if (dialog.buttons.start.disabled)
                return false;

            dialog.buttons.start.disabled = true;
            dialog.buttons.stop.style.removeProperty('display');
            dialog.buttons.save.style.display = 'none';

            dialog.buttons.start.innerText = '';
            dialog.buttons.start.appendChild(loadingImage());

            doCountDrug();

            return false;
        }

        done = true;
        fetchJSON('/api/command?command=c');
        resolveFunc({ result: resp.value === 'save', count});
        return true;
    });

    return new Promise((resolve, reject) => { resolveFunc = resolve; })
}

bindClick(footerButton, evt => { currentPage.onFooterNav(); });
bindClick('.nav-button', evt => { uiNav(evt.target.getAttribute('data-dest')); });

bindClick('.shutdown-button', async evt => {
    let resp = await new ModalDialog('Shutdown').text(SHUTDOWN_TEXT).button('Continue').button('Cancel').show();
    if (resp.value === 'continue') {
        await fetchJSON('/api/system/shutdown', 'POST', {});
    }
});

bindClick('.reboot-button', async evt => {
    let resp = await new ModalDialog('Reboot').text('Are you sure you want to reboot?').yes().no().show();
    if (resp.value === 'yes') {
        await fetchJSON('/api/system/reboot', 'POST', {});
    }
});

bindClick('.reset-doses', async evt => {
    let resp = await new ModalDialog('Reset Doses').text('Are you sure you want to reset all scheduled doses?').yes().no().show();
    if (resp.value === 'yes') {
        await fetchJSON('/api/schedule/reset', 'POST', {});
        cancelPendingCheck();
        startScheduleCheck();
        uiNavRoot();
    }
});

bindClick('.clear-doses', async evt => {
    let resp = await new ModalDialog('Clear Pending').text('Are you sure you want to clear all pending doses?').yes().no().show();
    if (resp.value === 'yes') {
        await fetchJSON('/api/schedule/clear', 'POST', {});
        cancelPendingCheck();
        startScheduleCheck();
        uiNavRoot();
    }
});

bindClick(scrollBack, evt => {
    let elem = currentPage.elem;
    elem.scrollBy({top: -elem.clientHeight, behavior: 'instant'});
});

bindClick(scrollFwd, evt => {
    let elem = currentPage.elem;
    elem.scrollBy({top: elem.clientHeight, behavior: 'instant'});
});
