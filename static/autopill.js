async function updateObject(objtype, id, updates, elem) {
    if (elem)
        elem.disabled = true;
    try {
        let resp = await fetchJSON(`/api/${objtype}/${id}`, 'PUT', updates);
    } catch (e) {
        await new ModalDialog('Error').text(`Error saving: ${e}`).ok().show();
    } finally {
        if (elem)
            elem.disabled = false;
    }
}

const activeCheckbox = objtype => (tbl, obj) => {
    let elem = ce('input', {type: 'checkbox'}, []);
    if (obj.active === 'true' || obj.active === true) elem.setAttribute('checked', 1);


    bindEvent(elem, 'input', evt => {
        obj.active = elem.checked ? 'true' : 'false';
        updateObject(objtype, obj.id, {'active': elem.checked}, elem);
    });
    return [elem];
};

function canonicalizeNDC(val) {
    let m = /(\d{4,5})-?(\d{3,4})-?(\d{1,2})/.exec(val);
    if (!m)
        return null;

    let new_ndc = (
        rjust(m[1], 5, '0') + '-' +
        rjust(m[2], 4, '0') + '-' +
        rjust(m[3], 2, '0')
    );
    if (new_ndc.length > 13)
        return null;
    return new_ndc;
}
