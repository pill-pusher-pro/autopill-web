const LOADING = '/static/loading.svg';

const PARAM_TYPE = {
    str: {
        tostr: v => v || '',
        fromstr: v => v || null,
    },
    strnn: {
        tostr: v => v || '',
        fromstr: v => v,
    },
    int: {
        tostr: v => (v === null || v === undefined) ? '' : `${v}`,
        fromstr: v => v === '' ? null : parseInt(v)
    },
    bool: {
        tostr: v => (v === null || v === undefined) ? '' : `${v}`,
        fromstr: v => {
            v = v.toLowerCase();
            if (v === '') return null;
            if (v === 'true') return true;
            if (v === 'false') return false;
            return parseInt(v) ? true : false;
        },
        element (tbl, param, val) {
            let params = {type: 'checkbox', value: val == 'true' ? '1' : ''};
            if (val.toLowerCase() == 'true' || val == '1') params['checked'] = '1';
            return ce('input', params, []);
        },
        fromelem(elem) {
            return elem.checked;
        }
    },
    date: {
        tostr: v => (v === null || v === undefined) ? '' : `${v}`,
        fromstr: v => v || null,
        element (tbl, param, val) {
            let params = {type: 'date', value: val};
            return ce('input', params, []);
        }
    }
};

function loadingImage() {
    return ce('img', {src: LOADING, width: '16', height: '16'});
}

class ObjectTable {
    constructor(defs) {
        this.baseuri = defs.baseuri;
        this.idfield = defs.idfield;
        this.objectDesc = defs.objectDesc;

        this.fields = defs.fields;
        this.params = defs.fields.filter(param => param.type !== 'none');
        this.columns = defs.fields.filter(param => param.showas === 'column');
        this.paramMap = {};

        this.includeConfig = defs.includeConfig;

        for (let param of defs.fields) {
            this.paramMap[param.name] = param;
        }

        this.objects = [];
        this.objectByID = {};

        let container = document.getElementById(defs.container);
        this.mainTable = container.querySelector('.object-table');
        this.mainTableBody = this.mainTable.querySelector('tbody');

        this.includeInactive = container.querySelector('.include-inactive');
        if (this.includeInactive) {
            this.includeInactive.addEventListener('input', evt => {
                this.refresh();
            })

        }

        container.querySelector('.new-button').addEventListener('click', evt => {
            let obj = this.createNewObject();
            this.editObject(obj, true);
        });
        container.querySelector('.refresh-button').addEventListener('click', evt => {
            this.refresh();
        });
        this.refresh();

        let thead = this.mainTable.querySelector('thead');
        let colgroup = this.mainTable.querySelector('colgroup');


        let tr = ce('tr', {}, [], thead);
        ce('col', {clas: 'actions'}, [], colgroup);
        ce('th', {}, [], tr);

        for (let param of this.columns) {
            ce('th', {clas: param.name}, [param.display], tr);
            ce('col', {clas: param.name}, [], colgroup);
        }

        if (this.includeConfig) {
            ce('col', {clas: 'config'}, [], colgroup);
            ce('th', {}, ['Config'], tr);
        }

    }

    addTitle(obj) { return `Add New ${this.objectDesc}` }
    editTitle(obj) { return `Edit ${this.objectDesc} ${obj.id}` }

    editObject(obj, asNew) {
        let errmsg = ce('div', {clas: 'errormsg'}, ['\u00A0']);
        let saving = false;

        let title = asNew ? this.addTitle(obj) : this.editTitle(obj);

        let dialog = new ModalDialog(title);

        for (let param of this.params) {
            let type = PARAM_TYPE[param.type];
            let input, elem;

            if (type.element) {
                input = type.element(this, param, obj[param.name]);
            } else {
                input = ce('input', {type: 'text', value: obj[param.name]}, []);
            }
            if (param.buttons) {
                elem = ce('div', {clas: 'object-field-buttons'}, [input]);
                for (let btn of param.buttons) {
                    let btne = ce('button', {}, [btn.text], elem);
                    bindClick(btne, evt => {
                        evt.preventDefault();
                        evt.stopPropagation();
                        btn.func(input, obj, dialog);
                    });
                }
            } else {
                elem = input;
            }
            dialog.addInput(param.name, input);
            dialog.label(param.display, elem);
            if (param.validate)
                dialog.validate(param.name, param.validate);
        }
        dialog.onvalidate(valid => {
            dialog.buttons.save.disabled = !valid || saving;
        });
        dialog.elem(errmsg);

        if (!asNew) {
            dialog.button('Copy');
            dialog.buttonspace();
        }

        dialog.button('', 'save').cancel();
        dialog.onshow(elem => {
            let btn = dialog.buttons.save;
            ce('div', {clas: 'saving-indicator'}, [
                loadingImage()
            ], btn);
            ce('div', {clas: 'saving-text'}, ['Save'], btn);

            for (let param of this.params) {
                if (param.canonicalize) {
                    dialog.inputs[param.name].addEventListener('blur', evt => {
                        evt.target.value = param.canonicalize(evt.target.value);
                    });
                }
                if (param.type === 'int') {
                    dialog.inputs[param.name].setAttribute('type', 'number');
                }
                if (param.maxlen !== undefined) {
                    dialog.inputs[param.name].setAttribute('maxlength', param.maxlen.toString());
                }
                if (param.minval !== undefined) {
                    dialog.inputs[param.name].setAttribute('min', param.minval.toString());
                }
                if (param.maxval !== undefined) {
                    dialog.inputs[param.name].setAttribute('max', param.maxval.toString());
                }

            }

        });

        let do_save = async () => {
            let conflicts = [];
            let btn = dialog.buttons.save;
            errmsg.innerText = '\u00A0';
            btn.classList.add('saving');
            saving = true;
            btn.disabled = true;
            try {
                let method = 'PUT';
                let uri;
                let body = {config: {}};
                for (let param of this.params) {
                    let input = dialog.inputs[param.name];
                    let type = PARAM_TYPE[param.type];
                    let value;
                    if (type.fromelem) {
                        value = type.fromelem(input);
                    } else {
                        value = input.value;
                        if (param.canonicalize)
                            input.value = value = param.canonicalize(value);
                        value = type.fromstr(value);
                    }

                    if (param.inconfig) {
                        body.config[param.name] = value;
                    } else {
                        body[param.name] = value;
                    }
                }

                if (asNew) {
                    method = 'CREATE';
                    uri = this.baseuri;
                    body[this.idfield] = null;
                } else {
                    uri = `${this.baseuri}/${obj.id}`;
                }

                let jsdata = await this.request(uri, method, body);
                if (jsdata.conflicts && jsdata.conflicts.length) {
                    conflicts = jsdata.conflicts;
                    let msg = jsdata.conflicts.map(([field, val, other]) => `${field} used by ${other}`).join('; ');
                    throw new Error(msg);
                } else {
                    if (!asNew) {
                        delete this.objectById[obj.id];
                    }

                    this.updateData(obj, jsdata);
                    this.buildRow(obj);

                    if (asNew) {
                        this.mainTableBody.appendChild(obj.tr);
                        this.objects.push(obj);
                    }

                    let text = asNew ? 'Created' : 'Saved';
                    add_notification(`${text} ${this.objectDesc} ${obj.id}`);
                    dialog.exit('saved');
                }
            } catch (e) {
                let text = e.toString();
                errmsg.innerText = text;
            } finally {
                saving = false;
                btn.classList.remove('saving');
                dialog.checkValidate();

                // Have to do this after running checkValidate, because that method clears the
                // 'validate-error' class.
                for (let [field, val, other] of conflicts) {
                    let error_input = dialog.inputs[field];
                    if (error_input) {
                        error_input.focus();
                        error_input.select();
                        error_input.classList.add('validate-error');
                    }
                }
            }
        };

        dialog.show(resp => {
            if (resp.value === 'save' || resp.value === 'enter') {
                if (!dialog.buttons.save.disabled) {
                    do_save();
                }
                return false;
            } else if (resp.value === 'copy') {
                let btn = dialog.buttons.copy;
                btn.parentElement.removeChild(btn);
                asNew = true;
                obj = this.createNewObject();
                dialog.setTitle(this.addTitle(obj));
                return false;
            }
            return true;
        });
    }

    async request(uri, method, body) {
        let jsdata = await fetchJSON(uri, method, body);
        if (jsdata.error && !jsdata.conflicts) {
            throw new Error(jsdata.error);
        }
        return jsdata;
    }

    createNewObject() {
        let tr = ce('tr', {}, []);
        let obj = {
            tr, config: {}
        };

        for (let param of this.params) {
            obj[param.name] = param.default || '';
        }

        tr.addEventListener('dblclick', evt => {
            evt.preventDefault();
            editObject(obj, false);
        });
        this.objects.push(obj);
        return obj;
    }

    updateData(obj, objdata) {
        for (let param of this.params) {
            let obj_value;
            if (param.get) {
                obj_value = param.get(objdata);
            } else if (param.inconfig) {
                obj_value = objdata.config[param.name];
            } else {
                obj_value = objdata[param.name];
            }
            let value = PARAM_TYPE[param.type].tostr(obj_value);
            if (param.canonicalize)
                value = param.canonicalize(value);
            obj[param.name] = value;
        }

        obj.config = objdata.config;
        obj.id = objdata[this.idfield];

        this.objectById[obj.id] = obj;
    }

    buildActions(obj, elem) {
        let edit_btn = ce('button', {clas: 'editbtn'}, ['Edit'], elem);
        bindClick(edit_btn, evt => {
            evt.preventDefault();
            this.editObject(obj, false);
        });
        //let del_btn = ce('button', {clas: 'delbtn'}, ['Del'], elem);

        /*del_btn.addEventListener('click', evt => {
            evt.preventDefault();
            this.askDeleteObject(obj);
        });*/
    }

    buildRow(obj) {
        let tr = obj.tr;
        tr.innerHTML = '';

        let actions = ce('td', {clas: 'actions'}, [], tr);
        let actions_div = ce('div', {}, [], actions);

        this.buildActions(obj, actions_div);

        for (let param of this.columns) {
            ce('td', {clas: param.name},
               param.element ? param.element(this, obj) : [obj[param.name]], tr);
        }

        if (this.includeConfig) {
            let td = ce('td', {clas: 'config'}, [], tr);
            let configdiv = ce('div', {}, [], td);
            for (let [key, val] of Object.entries(obj.config || {})) {
                let param = this.paramMap[key];
                if (!param || !param.inconfig || param.showas === 'config') {
                    ce('span', {clas: 'configpair'}, [
                        ce('span', {clas: 'configkey'}, [key]),
                        ce('span', {clas: 'configvalue'}, [`${val}`])
                    ], configdiv);
                }
            }
            if (!td.firstChild) {
                append_text(td, '\u00A0');
            }
        }
    }

    getRefreshUri() {
        let uri = this.baseuri;
        if (this.includeInactive && this.includeInactive.checked) {
            uri +=  '?include_inactive=1'
        }
        return `${uri}`;
    }

    refreshData(jsdata) {
        this.objects = [];
        this.objectById = {};

        this.mainTableBody.innerHTML = '';
        for (let objdata of jsdata.results) {
            let tr = ce('tr', {}, [], this.mainTableBody);
            let obj = { tr };

            tr.addEventListener('dblclick', evt => {
                evt.preventDefault();
                this.editObject(obj, false);
            });

            this.objects.push(obj);
            this.updateData(obj, objdata);
            this.buildRow(obj);
        }

    }

    async refresh() {
        let resp = await fetch(this.getRefreshUri());
        let jsdata = await resp.json();
        this.refreshData(jsdata);
    }
};
