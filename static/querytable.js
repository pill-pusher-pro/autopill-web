/* global create_element append_text add_magic bugtrack ModalDialog
 * schedule_update_running setclass build_query compare_obj short_timestamp iso_timestamp
 * duration_text
*/

const COMMON_COLUMNS = {
    selectind: {
        label: '',
        val() { return ''; }
    },
    idcolumn: {
        label: 'ID',
        val(run, td, opt) {
            setclass(td, 'numeric', true);
            return ''+opt.get_row_id(run);
        }
    },
    timestamp: {
        label: 'Time',
        val(run, td, opt) {
            let dt = new Date(run.begin_time * opt.time_mul);
            let text = opt.short_time ? short_timestamp(dt) : iso_timestamp(dt);
            if (opt.link_time) {
                td.appendChild(create_element('a', text, '', {href: run.linkbase, target:'_blank'}));
            } else {
                return text;
            }
            return null;
        }
    },
};


class QueryTable {
    constructor(columndef, buttondef, baseurl, tableid, columns, details_div, search_div) {
        this.columndef = columndef;
        this.buttondef = buttondef;
        this.baseurl = baseurl;
        this.table = document.getElementById(tableid);
        this.columns = columns;

        this.details_div = document.getElementById(details_div);
        this.search_div = document.getElementById(search_div);

        this.buttons = {};

        if (this.search_div) {
            this.buttondef.forEach(name => {
                let func = name.replace(/-/g, '_');
                let btn = this.search_div.querySelector('.' + name);
                this.buttons[name] = btn;
                if (btn) {
                    btn.addEventListener('click', evt => this[func](evt));
                }
            });
            this.setup_search();
        }

        this.lastid = '';
        this.direction = 'before';

        this.min_id = '';
        this.max_id = '';

        this.query_limit = 30;
        this.selected_row = null;
        this.selected_row_elem = null;
        this.selected_row_detail = null;
        this.row_data = [];

        this.width_mul = 1.0;
        this.allow_select = true;

        this.making_request = false;
        this.keep_index = true;

        this.query_state = {};

        this.predefined_rows = [];

    }

    ensure_predefined_rows(max) {
        for (let i = this.predefined_rows.length; i < max; i++) {
            let tbody = this.table.querySelector('tbody');
            let row = {
                tr: create_element('tr', '', 'queryrow hiddenrow'),
                cells: {}
            };
            tbody.appendChild(row.tr);
            for (let colname of this.columns) {
                let col = this.get_columndef(colname);
                if (!col) {
                    console.error('column undefined', colname);
                }
                let td = create_element('td', '', colname);
                row.tr.appendChild(td);
                row.cells[colname] = td;
            }
            if (this.allow_select) {
                let index = i;
                row.tr.addEventListener('click', (evt) => {
                    this.select_row(i);
                });
                row.tr.addEventListener('dblclick', (evt) => {
                    let row = this.row_data[index];
                    evt.preventDefault();
                    evt.stopPropagation();
                    if (row)
                        this.on_dblclick_row(row, evt);
                    if (window.getSelection) {
                        if (window.getSelection().empty) {
                            window.getSelection().empty();
                        } else if (window.getSelection().removeAllRanges) {
                            window.getSelection().removeAllRanges();
                        }
                    }
                });
            }
            this.predefined_rows.push(row);
        }
    }

    get_columndef(name) {
        return this.columndef[name] || COMMON_COLUMNS[name];
    }

    setup_search() {
    }

    on_dblclick_row(row, evt) {
    }

    create_row_custom(row, tr) {
    }

    create_row(row) {
        this.ensure_predefined_rows(row.index + 1);
        let pdrow = this.predefined_rows[row.index];
        let tr = pdrow.tr;
        setclass(tr, 'hiddenrow', false);

        this.create_row_custom(row, tr);

        for (let colname of this.columns) {
            let col = this.get_columndef(colname);
            let td = pdrow.cells[colname];
            td.innerText = '';
            let text = col.val(row, td, this);
            if (text) append_text(td, text);
        }

        let td, link, text;

        return tr;
    }

    update_rows(rows) {
        this.selected_row = null;
        this.clear_details();
        if (this.details_div)
            this.details_div.innerHTML = '';

        this.row_data = rows;

        let index = 0;
        for (let row of rows) {
            row.index = index++;
            row.row_elem = this.create_row(row);
            if (this.details_div) {
                row.detail_elem = create_element('div', '', 'querytable-details-card');
                this.details_div.appendChild(row.detail_elem);
                this.build_row_details(row, row.detail_elem);
            }
        }
        if (rows.length) {
            this.min_id = this.get_row_id(rows[rows.length - 1]);
            this.max_id = this.get_row_id(rows[0]);
        }
        for (let i = rows.length; i < this.predefined_rows.length; i++) {
            setclass(this.predefined_rows[i].tr, 'hiddenrow', true);
        }
    }

    build_row_details(row, elem) {
    }

    build_headers() {
        let flexcols = [];
        let fixwidth = 0;
        let flexwidth = 0;

        let colgroup = this.table.querySelector('colgroup');
        colgroup.innerHTML = '';

        for (let colname of this.columns) {
            let col = this.get_columndef(colname);
            let elem = create_element('col', '', colname);
            colgroup.appendChild(elem);

        }

        let thead = this.table.querySelector('thead');
        thead.innerHTML = '';

        if (this.overheader) {
            let overheader_row = create_element('tr');

            let last_group = null;
            let last_group_elem = null;
            let group_span = 0;

            for (let colname of this.columns) {
                let col = this.get_columndef(colname);
                let group = col.group || '';
                if (group === last_group) {
                    group_span++;
                } else {
                    if (last_group_elem) {
                        last_group_elem.setAttribute('colspan', ''+group_span);
                    }
                    let th = create_element('th', group);
                    overheader_row.appendChild(th);
                    last_group = group;
                    last_group_elem = th;
                    group_span = 1;
                }
            }
            if (last_group_elem) {
                last_group_elem.setAttribute('colspan', ''+group_span);
                thead.appendChild(overheader_row);
            }
        }

        let header_row = create_element('tr');
        thead.appendChild(header_row);
        for (let colname of this.columns) {
            let col = this.get_columndef(colname);
            let th = create_element('th', col.label, colname);
            header_row.appendChild(th);
        }

    }

    async do_fetch() {
    }

    get_query_params() {
        return {};
    }

    get_row_id(row) {
        return row.id;
    }

    get_rows_from_query(data) {
    }

    async fetch_logs(query_state) {
        if (this.making_request)
            return;

        try {
            this.making_request = true;

            while (true) {
                let query = build_query(query_state);

                let resp = await fetch(`${this.baseurl}?${query}`);
                let data = await resp.json();

                let selected_id = this.selected_row ? this.get_row_id(this.selected_row) : null;

                this.update_rows(this.get_rows_from_query(data));

                let index = this.row_data.findIndex(row => this.get_row_id(row) == selected_id);

                if (this.keep_index && index !== -1) {
                    this.select_row(index);
                } else if (this.direction == 'before') {
                    this.select_row(0);
                } else {
                    this.select_row(this.row_data.length - 1);
                }
                this.keep_index = true;
                if (compare_obj(this.query_state, query_state))
                    return;
                query_state = this.query_state;
            }
        } finally {
            this.making_request = false;
        }
    }

    refresh_logs(keep_index) {
        if (!keep_index) {
            this.keep_index = false;
        }
        let showall = this.show_all_chk ? this.show_all_chk.checked : false;

        let params = this.get_query_params();

        params.lastid = this.lastid;
        params.direction = this.direction;
        params.limit = this.query_limit;
        params.comp = 1;

        let new_query_state = params;
        this.query_state = new_query_state;
        this.fetch_logs(new_query_state).catch(console.error);
    }

    refresh() { this.refresh_logs(true); }

    async fetch_row_details(row) {
    }

    select_row(index) {
        if (!this.allow_select) return;
        let row = this.row_data[index];
        if (!row) return;

        this.selected_row = row;

        if (row.row_elem !== this.selected_row_elem) {
            if (this.selected_row_elem)
                setclass(this.selected_row_elem, 'selected', false);
            setclass(row.row_elem, 'selected', true);
            this.selected_row_elem = row.row_elem;
        }
        if (row.detail_elem !== this.selected_detail_elem) {
            if (this.selected_detail_elem)
                setclass(this.selected_detail_elem, 'selected', false);
            if (row.detail_elem)
                setclass(row.detail_elem, 'selected', true);
            this.selected_detail_elem = row.detail_elem;
        }
        this.fetch_row_details(row).catch(console.error);
    }

    on_key(evt, key_full) {
        return false;
    }

    key_r() { this.move_newest(); }
    key_g() { this.refresh_logs(true); }
    key_ArrowUp() { this.move_up(); }
    key_ArrowDown() { this.move_down(); }
    key_ArrowLeft() { this.move_older(); }
    key_ArrowRight() { this.move_newer(); }

    key(evt) {
        let key = evt.key;

        let key_full = '';
        if (evt.ctrlKey) key_full += 'ctrl_';
        if (evt.altKey) key_full += 'alt_';
        if (evt.shiftKey) key_full += 'shift_';
        key_full += key;

        if (this.on_key(evt, key_full))
            return true;

        let method = this['key_' + key_full];
        if (method) {
            if (method.apply(this, [evt]) !== false) {
                return true;
            }
        }
        return false;
    }

    move_up() {
        if (this.selected_row == null) {
            if (this.row_data.length != 0) {
                this.select_row(this.row_data.length - 1);
                return;
            }
        } else {
            let index = this.selected_row.index;
            if (index > 0) {
                this.select_row(index - 1);
                return;
            }
        }
        this.move_newer();
    }

    move_down() {
        if (this.selected_row == null) {
            if (this.row_data.length != 0) {
                this.select_row(0);
                return;
            }
        } else {
            let index = this.selected_row.index;
            if (index < this.row_data.length - 1) {
                this.select_row(index + 1);
                return;
            }
        }
        this.move_older();
    }

    move_older() {
        this.direction = 'before';
        this.lastid = this.max_id = this.min_id;
        this.max_id = this.min_id - 1;
        this.refresh_logs(false);
    }

    move_newer() {
        this.direction = 'after';
        this.lastid = this.max_id;
        this.min_id = this.max_id + 1;
        this.refresh_logs(false);
    }

    move_newest() {
        this.direction = 'before';
        this.lastid = '';
        this.refresh_logs(false);
    }

    move_oldest() {
        this.direction = 'after';
        this.lastid = '';
        this.refresh_logs(false);
    }

    clear_details() {
    }
};
