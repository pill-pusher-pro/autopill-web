'use strict';
/* global fetch setTimeout setInterval clearInterval create_element append_text iso_timestamp*/

const MAX_LINES = 10000;

const tabhdr = document.getElementById('tab-buttons');
const tabdivs = document.getElementById('tab-content');

let hasFocus = true;
let updateToken = null;

let keypress = {};

// ========================================================
// TABS
// ========================================================

let currentTab = null;

class MainTab extends Tab {
    constructor(title, id) {
        super(title, id);
        this.tabElem = document.getElementById('maintab');
    }

    onSelect() {

    }

    update() {
    }
}

class LogTab extends Tab {
    constructor(title, id) {
        super(title, id);
        this.tabElem = document.getElementById('logtab');
        this.logElem = document.getElementById('livelog');
        this.logLines = [];
        this.fetching = false;
    }

    add_log_element(logline) {
        let row = create_element('span', '', 'logline');
        let time = new Date(logline.time);
        row.appendChild(create_element('span', iso_timestamp(time), 'time'));
        append_text(row, ' ');
        row.appendChild(create_element('span', logline.level, logline.level.toLowerCase()));
        append_text(row, ' ');
        row.appendChild(create_element('span', `[${logline.name}]`, 'log-module'));
        append_text(row, ' ');
        convert_urls_to_links(row, logline.msg);
        append_text(row, ' \n');

        logline.elem = row;
        this.logElem.appendChild(row);
    }

    async fetch_lines() {
        let since = 0;
        if (this.logLines.length > 0) {
            since = this.logLines[this.logLines.length - 1].time;
        }

        let resp = await fetch(`/livelog/logs?since=${since}`);
        let data = await resp.json();

        if (data.log.length == 0)
            return;

        let body = this.tabElem;
        let is_at_bottom = body.scrollTop >= (body.scrollHeight - body.clientHeight - 75);

        for (let line of data.log) {
            this.add_log_element(line);
            this.logLines.push(line);
        }

        if (this.logLines.length >= MAX_LINES) {
            let splitpoint = this.logLines.length - MAX_LINES;
            let removed_lines = this.logLines.splice(0, splitpoint);
            for (let line of removed_lines) {
                this.logElem.removeChild(line.elem);
            }
        }

        if (is_at_bottom) {
            body.scrollTop = body.scrollHeight - body.clientHeight;
        }
    }

    async do_fetch() {
        if (this.fetching) return;
        this.fetching = true;
        try {
            await this.fetch_lines();
        } finally {
            this.fetching = false;
        }
    }

    onActivate() {
        this.update();
    }

    update() {
        this.do_fetch().catch(console.error);
    }
}

const USER_DATA = {
    idfield: 'userid',
    baseuri: '/api/user',
    objectDesc: 'User',

    container: 'usertab',

    includeConfig: true,

    fields: [
        {
            name: 'id',
            type: 'none',
            display: 'ID',
            showas: 'column',
        },
        {
            name: 'active',
            type: 'bool',
            display: 'Active',
            showas: 'column',
            default: 'true',
            element: activeCheckbox('user'),
        },
        {
            name: 'name',
            display: 'Name',
            type: 'str',
            maxlen: 15,
            validate: value => value.length > 0 && value.length <= 15,
            showas: 'column',
        },
        {
            name: 'led',
            display: 'LED',
            type: 'int',
            min: -1,
            max: 3,
            showas: 'column',
        },
    ]
};

class UserTable extends ObjectTable {
    constructor() {
        super(USER_DATA);
    }
};

class UserTab extends Tab {
    constructor(title, id) {
        super(title, id);
        this.tabElem = document.getElementById('usertab');
        this.objectTable = new UserTable();
    }

    onKey(evt) {
        //return user_table.key(evt);
    }

    onSelect() {
        this.objectTable.refresh();
    }

    update() {
    }
};

const DRUG_DATA = {
    idfield: 'drugid',
    baseuri: '/api/drug',
    objectDesc: 'Drug',

    container: 'drugtab',

    includeConfig: false,
    fields: [
        {
            name: 'id',
            type: 'none',
            display: 'ID',
            showas: 'column',
        },
        {
            name: 'active',
            type: 'bool',
            display: 'Active',
            showas: 'column',
            default: 'true',
            element: activeCheckbox('drug'),
        },
        {
            name: 'name',
            display: 'Name',
            type: 'strnn',
            maxlen: 60,
            validate: value => value.length > 0 && value.length <= 60,
            showas: 'column',
        },
        {
            name: 'strength',
            display: 'Strength (µg)',
            type: 'int',
            min: 0,
            max: 10000000,
            showas: 'column',
        },
        {
            name: 'count',
            display: 'Count',
            type: 'int',
            min: 0,
            max: 10000000,
            showas: 'column',
            buttons: [
                {text: "+30", func: input => input.value = parseInt(input.value) + 30},
                {text: "+60", func: input => input.value = parseInt(input.value) + 60},
                {text: "+90", func: input => input.value = parseInt(input.value) + 90},
            ],
        },
        {
            name: 'reorder_point',
            type: 'int',
            display: 'Reorder point',
            showas: 'column',
            min: -1,
            max: 1000,
            default: '',
        },
        {
            name: 'ndc',
            display: 'NDC',
            type: 'str',
            maxlen: 13,
            validate: v => v === '' || canonicalizeNDC(v) !== null,
            canonicalize: v => v === '' ? '' : canonicalizeNDC(v) || v,
            element(tbl, v) { return [canonicalizeNDC(v.ndc) || v.ndc] },
            showas: 'column',
        },
        {
            name: 'hopper_index',
            type: 'int',
            display: 'Hopper',
            showas: 'column',
            default: '',
        },
        {
            name: 'notes',
            display: 'Notes',
            type: 'strnn',
            maxlen: 1000,
            showas: 'column',
        },
    ]
};

class DrugTable extends ObjectTable {
    constructor() {
        super(DRUG_DATA);
    }

    buildActions(obj, elem) {
        super.buildActions(obj, elem);
        let btn = ce('button', {}, ['Count'], elem);
        if (obj.hopper_index === '') {
            btn.disabled = true;
        } else {
            bindClick(btn, evt => {
                countDrug(obj);
            });
        }
    }
};

class DrugTab extends Tab {
    constructor(title, id) {
        super(title, id);
        this.tabElem = document.getElementById('drugtab');
        this.objectTable = new DrugTable();
    }

    onKey(evt) {
        //return drug_table.key(evt);
    }

    onSelect() {
        this.objectTable.refresh();
    }

    update() {
    }
};

function objectRefType(listField, mapField, idField, nameField) {
    return {
        tostr: v => (v === null || v === undefined) ? '' : `${v}`,
        fromstr: v => v === '' ? null : parseInt(v),
        element (tbl, name, val) {
            let select = ce('select', {}, []);
            for (let obj of tbl[listField]) {
                let option = ce('option', {value: obj[idField]}, [obj[nameField]], select);
                if (val == obj[idField]) {
                    option.setAttribute('selected', '1');
                }
            }
            return select;
        }
    }
}

PARAM_TYPE.user = objectRefType('allUsers', 'userById', 'userid', 'name');
PARAM_TYPE.drug = objectRefType('allDrugs', 'drugById', 'drugid', 'name');

const SCHEDULE_DATA = {
    idfield: 'scheduleid',
    baseuri: '/api/schedule',
    objectDesc: 'Schedule',

    container: 'scheduletab',

    includeConfig: false,

    fields: [
        {
            name: 'id',
            type: 'none',
            display: 'ID',
            showas: 'column',
        },
        {
            name: 'active',
            type: 'bool',
            display: 'Active',
            showas: 'column',
            default: 'true',
            element: activeCheckbox('schedule'),
        },
        {
            name: 'userid',
            type: 'user',
            display: 'User',
            showas: 'column',
            element(tbl, obj) {
                return [tbl.userById[obj.userid].name];
            }
        },
        {
            name: 'drugid',
            type: 'drug',
            display: 'Drug',
            showas: 'column',
            element(tbl, obj) {
                return [tbl.drugById[obj.drugid].name];
            }
        },
        {
            name: 'count',
            type: 'int',
            display: 'Count',
            showas: 'column',
            min: 1,
            max: 1000,
            default: '',
        },
        {
            name: 'start_date',
            display: 'Start date',
            type: 'date',
            maxlen: 10,
            validate: value => value.length > 0 && value.length <= 10,
            showas: 'column',
            get: obj => obj.start_date_iso,
        },
        {
            name: 'time',
            display: 'Time',
            type: 'str',
            maxlen: 10,
            showas: 'column',
            get: obj => obj.time_text,
        },
        {
            name: 'date_filter',
            display: 'Date Filter',
            type: 'strnn',
            maxlen: 1000,
            showas: 'column',
        },
    ]
};

class ScheduleTable extends ObjectTable {
    constructor() {
        super(SCHEDULE_DATA);
    }
    async refresh() {
        let jsdata = await Promise.all([
            fetchJSON(this.getRefreshUri(), 'GET'),
            fetchJSON('/api/user', 'GET'),
            fetchJSON('/api/drug', 'GET')
        ]);
        this.userById = {};
        this.drugById = {};
        this.allUsers = jsdata[1].results.filter(o => o.active);
        this.allDrugs = jsdata[2].results.filter(o => o.active);
        for (let obj of this.allUsers) { this.userById[obj.userid] = obj; }
        for (let obj of this.allDrugs) { this.drugById[obj.drugid] = obj; }
        this.refreshData(jsdata[0]);
        console.log(this);
    }
};

class ScheduleTab extends Tab {
    constructor(title, id) {
        super(title, id);
        this.tabElem = document.getElementById('scheduletab');
        this.objectTable = new ScheduleTable();
    }

    onKey(evt) {
        //return schedule_table.key(evt);
    }

    onSelect() {
        this.objectTable.refresh();
    }

    update() {
    }
};

let tabs = [
    new MainTab('1. Main', 'main'),
    new LogTab('2. Console', 'console'),
    new DiagTab('3. Diagnostics', 'diag'),
    new UserTab('4. Users', 'users'),
    new DrugTab('5. Drugs', 'drugs'),
    new ScheduleTab('6. Schedule', 'schedule'),
];

function stopEvent(evt) {
    evt.stopPropagation();
    evt.stopImmediatePropagation();
    evt.preventDefault();
}

function selectTab(newTab) {
    if (newTab != currentTab) {
        if (currentTab !== null) {
            currentTab.tabElem.style.visibility = 'hidden';
            currentTab.tabButton.className = '';
            currentTab.onDeactivate();
        }
        currentTab = newTab;
        currentTab.tabElem.style.visibility = 'visible';
        currentTab.tabButton.className = 'selected';
        setTimeout(() => currentTab.tabElem.focus(), 50);

        let url = location.href;
        let newurl = url.split('#')[0] + '#' + currentTab.id;
        if (url != newurl) {
            history.replaceState(null, '', newurl);
        }

        currentTab.onActivate();
    }
    currentTab.onSelect();
    setTimeout(() => currentTab.tabElem.focus(), 50);
}

function add_header_button(text, bind, func) {
    let button = create_element('button', text);
    button.addEventListener('click', func);
    if (bind) {
        keypress[bind] = func;
    }
    tabhdr.appendChild(button);
}

function add_opener_button(text, bind, url) {
    add_header_button(text, bind, evt => {
        window.open(url, '_blank');
    });
}

function init() {
    let tab_by_id = {};
    for (let tab of tabs) {
        tab_by_id[tab.id] = tab;
        tab.tabElem.style.visiblity = 'hidden';
        tabhdr.appendChild(tab.tabButton);
    }
    let start_tab = tab_by_id[location.hash.substr(1)];
    if (!start_tab)
        start_tab = tabs[0];
    selectTab(start_tab);

    document.addEventListener("visibilitychange", evt => {
        checkVisibility();
        currentTab.checkAutoUpdate();
    });

    tabs.forEach((tab, idx) => {
        keypress[`${idx + 1}`] = evt => selectTab(tab);
    });

    checkVisibility();
}

function checkVisibility() {
    if (document.hidden) {
        if (updateToken !== null) {
            clearInterval(updateToken);
            updateToken = null;
        }
    } else {
        if (updateToken === null) {
            updateToken = setInterval(updateTick, 500);
            updateTick();
        }
    }
}

function updateTick() {
    currentTab.update();
}

function processKey(evt, type) {
    let tgt = evt.target;
    if (tgt && (tgt.nodeName == 'INPUT' || tgt.nodeName == 'TEXTAREA' || tgt.nodeName == 'SELECT'))
        return false;

    let key = keyName(evt);
    let func;
    if (type == 'press') {
        func = currentTab.keybind_press[key];
        if (!func) func = keypress[key];
    } else if (type == 'down') {
        console.log('down', key);
        func = currentTab.keybind_down[key];
    } else if (type == 'up') {
        func = currentTab.keybind_up[key];
    }
    if (func) {
        func(evt);
        return true;
    }
    return false;
}

function keyHandler(evt, press) {
    if (processKey(evt, press)) {
        stopEvent(evt);
        return true;
    }
    if (press == 'down') {
        if (currentTab.onKey(evt)) {
            stopEvent(evt);
            return true;
        }
    }
    return false;
}

if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', init);
} else {
    init();
}

window.addEventListener('keydown', evt => keyHandler(evt, 'down'));
window.addEventListener('keyup', evt => keyHandler(evt, 'up'));
window.addEventListener('keypress', evt => keyHandler(evt, 'press'));
window.addEventListener('focus', evt => { hasFocus = true; });
window.addEventListener('blur', evt => { hasFocus = false; });
window.addEventListener('resize', evt => { currentTab.onResize(); });
