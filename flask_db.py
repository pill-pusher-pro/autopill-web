import sys
import logging
import time
import datetime

from flask import send_from_directory, request, abort
from ktpanda.threadpool import get_main_pool
from ktpanda.dateutils import localtime_to_unix

from flask_utils.utils import tojson, get_request_int

from flask_main import app
from date_filter import compile_filter
from utils import parsedate
from database import Drug, User, Schedule, ScheduleExpand
from database import withdb, get_pending_schedules, reset_schedules, adjust_drug_count
from autopill_interface import AutopillInterface

import database


log = logging.getLogger(__name__)

def _list_objects(db, cls, **args):
    if request.method == 'CREATE':
        new_fields = request.get_json(force=True, cache=False)
        res = cls.put(db, None, new_fields)
        return tojson(res)

    res = cls.list(db, get_request_int('include_inactive'), **args)
    return tojson({
        'results': [obj.fields for obj in res]
    })

def _get_put_object(db, cls, id=None):
    if request.method == 'GET':
        res = cls.get(db, id)
        if res is None:
            abort(404)
        return tojson(res.fields)
    elif request.method == 'PUT':
        new_fields = request.get_json(force=True, cache=False)
        res = cls.put(db, id, new_fields)
        if res is None:
            abort(404)
        return tojson(res)
    else:
        return tojson({})

@app.route('/api/drug', methods=['GET', 'CREATE'])
@withdb
def list_drug(db):
    return _list_objects(db, Drug, ndc=request.args.get('ndc'), hopper_index=get_request_int('hopper_index'))

@app.route('/api/drug/<int:id>', methods=['GET', 'PUT', 'DELETE'])
@withdb
def get_put_drug(db, id):
    return _get_put_object(db, Drug, id)

@app.route('/api/drug/reorder', methods=['GET'])
@withdb
def drug_reorder(db):
    return tojson({
        'results': database.drugs_needing_reorder(db)
    })

@app.route('/api/drug/<int:id>/adjust', methods=['POST'])
@withdb
def adjust_drug(db, id):
    data = request.get_json(force=True, cache=False)
    drug = adjust_drug_count(db, id, data['amount'])
    return tojson(drug.fields)


@app.route('/api/user', methods=['GET', 'CREATE'])
@withdb
def list_user(db):
    return _list_objects(db, User)

@app.route('/api/user/<int:id>', methods=['GET', 'PUT', 'DELETE'])
@withdb
def get_put_user(db, id):
    return _get_put_object(db, User, id)

@app.route('/api/schedule', methods=['GET', 'CREATE'])
@withdb
def list_schedule(db):
    if get_request_int('full'):
        cls = ScheduleExpand
    else:
        cls = Schedule
    return _list_objects(db, cls, userid=get_request_int('userid'), drugid=get_request_int('drugid'))

@app.route('/api/schedule/<int:id>', methods=['GET', 'PUT', 'DELETE'])
@withdb
def get_put_schedule(db, id):
    return _get_put_object(db, Schedule, id)

@withdb
def complete_drug_dispense(db, drugid, req):
    log.info(f'drug dispense {drugid} complete, result={req.result} msg={req.message} count={req.pill_count}')
    database.adjust_drug_count(db, drugid, -req.pill_count)


@app.route('/api/drug/<int:id>/dispense', methods=['POST'])
@withdb
def dispense_drug(db, id):
    drug = Drug.get(db, id)
    if not drug:
        abort(404)

    count = get_request_int('count', 1)
    if count < 1:
        abort(400)

    intf = AutopillInterface.instance
    req = intf.queue_dispense(
        drug.hopper_index,
        count,
        complete=lambda req: get_main_pool().run(complete_drug_dispense, drug.drugid, req)
    )
    log.info(f'start drug dispense {drug.drugid}, req id={req.id}')

    return tojson({
        'drug': drug.fields,
        'request': req.tojson()
    })

@withdb
def complete_schedule_dispense(db, now, date, sched, req):
    log.info(f'schedule dispense {sched.scheduleid} complete, result={req.result} msg={req.message} count={req.pill_count}')
    if req.result:
        database.add_dispense_history(db, int(now), sched.userid, sched.drugid, req.pill_count)
        Schedule.put(db, sched.id, {'last_dispense': date})

def get_time_params():
    now = time.time()
    ts = datetime.datetime.fromtimestamp(now)
    date = ts.date()
    tod = 60 * ts.hour + ts.minute
    return now, ts, date, tod

@app.route('/api/schedule/<int:id>/dispense', methods=['POST'])
@withdb
def dispense_schedule(db, id):
    sched = ScheduleExpand.get(db, id)
    if not sched:
        abort(404)

    now, ts, date, tod = get_time_params()

    # If current time is less than the scheduled time, count it as a dispense for the previous day.
    if tod < sched.time:
        date -= datetime.timedelta(days=1)

    intf = AutopillInterface.instance
    req = intf.queue_dispense(
        sched.hopper_index,
        sched.count,
        complete=lambda req: get_main_pool().run(complete_schedule_dispense, now, date, sched, req)
    )
    log.info(f'start schedule dispense {sched.scheduleid}, req id={req.id}')

    return tojson({
        'schedule': sched.fields,
        'request': req.tojson()
    })

@app.route('/api/schedule/<int:id>/clear', methods=['POST'])
@withdb
def clear_schedule(db, id):
    sched = ScheduleExpand.get(db, id)

    now, ts, date, tod = get_time_params()
    if tod < sched.time:
        date -= datetime.timedelta(days=1)

    Schedule.put(db, sched.id, {'last_dispense': date})
    return tojson({
        'schedule': sched.fields,
    })

@app.route('/api/schedule/<int:id>/reset', methods=['POST'])
@withdb
def reset_schedule(db, id):
    sched = ScheduleExpand.get(db, id)
    now, ts, date, tod = get_time_params()

    date -= datetime.timedelta(days=(2 if tod < sched.time else 1))

    Schedule.put(db, sched.id, {'last_dispense': date})
    return tojson({
        'schedule': sched.fields,
    })

@app.route('/api/schedule/clear', methods=['POST'])
@withdb
def clear_all_schedules(db):
    now, ts, date, tod = get_time_params()
    yesterday = date - datetime.timedelta(days=1)
    schedules = get_pending_schedules(db, ts)
    for sched in schedules:
        Schedule.put(db, sched['scheduleid'], {'last_dispense': yesterday if tod < sched.time else date})

    return tojson({
        'schedules': schedules,
    })

@app.route('/api/schedule/reset', methods=['POST'])
@withdb
def api_reset_pending_schedules(db):
    now = datetime.datetime.now()
    reset_schedules(db, now)
    return tojson({
        "ok": True
    })

@app.route('/api/history', methods=['GET'])
@withdb
def get_history(db):
    mintime = maxtime = None
    mindate = request.args.get('mindate')
    if mindate:
        dt = parsedate(mindate)
        mintime = localtime_to_unix(dt.year, dt.month, dt.day, 0, 0, 0)

    maxdate = request.args.get('maxdate')
    if maxdate:
        dt = parsedate(maxdate)
        maxtime = localtime_to_unix(dt.year, dt.month, dt.day, 23, 59, 59)

    userid = get_request_int('userid')

    limit = get_request_int('limit', 100)

    result = database.query_dispense_history(db, mintime=mintime, maxtime=maxtime, userid=userid, limit=limit)

    return tojson(
        {'results': result}
    )

@app.route('/api/history/undo', methods=['POST'])
@withdb
def undo_history(db):
    data = request.get_json(force=True, cache=False)
    res = database.undo_dispense_history(db, data['timestamp'], data['userid'], data['drugid'], data['count'])
    return tojson({'result': res})

@app.route('/api/schedule/pending', methods=['GET'])
@withdb
def api_get_pending_schedules(db):
    now = datetime.datetime.now()
    schedules = get_pending_schedules(db, now)

    return tojson({
        'pending': schedules
    })

@app.route('/api/uimain', methods=['GET'])
@withdb
def api_ui_main(db):
    now = datetime.datetime.now()
    return tojson({
        'pending': database.get_pending_schedules(db, now),
        'reorder': database.drugs_needing_reorder(db)
    })
