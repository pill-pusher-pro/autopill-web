import sys
import ast
import datetime
import functools
from utils import parsedate

WHITELIST_TYPES = {
    'Expression', 'Name', 'Attribute', 'Load',
    'Compare', 'Eq', 'NotEq', 'Gt', 'Lt', 'GtE', 'LtE',
    'BinOp', 'Add', 'Sub', 'Mult', 'Div', 'Mod',
    'UnaryOp', 'Not',
    'BoolOp', 'And', 'Or',
    'Call',
    'Constant'
}
WHITELIST_NAMES = {'date',}
WHITELIST_ATTRS = {'day', 'days', 'month', 'year', 'weekday'}

@functools.total_ordering
class SimpleDate:
    def __init__(self, date):
        self.date = date

    @property
    def day(self):
        return self.date.day

    @property
    def month(self):
        return self.date.day

    @property
    def year(self):
        return self.date.year

    @property
    def weekday(self):
        return self.date.weekday()

    def __eq__(self, o):
        if isinstance(o, datetime.date):
            return self.date == o
        return self.date == o.date

    def __lt__(self, o):
        if isinstance(o, datetime.date):
            return self.date < o
        return self.date < o.date

    def __add__(self, o):
        return SimpleDate(self.date + datetime.timedelta(days=o))

    def __sub__(self, o):
        if isinstance(o, int):
            SimpleDate(self.date - datetime.timedelta(days=o))
        return (self.date - o.date).days

    def __repr__(self):
        return repr(self.date)

class ValidationVisitor(ast.NodeTransformer):
    def visit(self, node):
        ntype = type(node).__name__
        if ntype not in WHITELIST_TYPES:
            raise ValueError(f'{ntype} not allowed')
        return super().visit(node)

    def visit_Name(self, node):
        if node.id not in WHITELIST_NAMES:
            raise ValueError(f'{node.id!r} is not a valid name')
        return self.generic_visit(node)

    def visit_Attribute(self, node):
        if node.attr not in WHITELIST_ATTRS:
            raise ValueError(f'{node.attr!r} is not a valid attribute')
        return self.generic_visit(node)

    def visit_Constant(self, node):
        if isinstance(node.value, str):
            return ast.Call(func=ast.Name(id='pd', ctx=ast.Load()), args=[node], keywords=[])
        return self.generic_visit(node)

def parse_filter(text):
    tree = ast.parse(text, mode='eval')
    #print(ast.dump(tree))
    ntree = ValidationVisitor().visit(tree)
    xtree = ast.Expression(body=ast.Lambda(
        args=ast.arguments(
            posonlyargs=[],
            args=[ast.arg(arg='date'), ast.arg(arg='pd')],
            kwonlyargs=[],
            kw_defaults=[],
            defaults=[]),
        body=ntree.body))
    return ast.fix_missing_locations(xtree)

def compile_filter(text):
    raw_filter = eval(compile(parse_filter(text), '<text>', mode='eval'))
    return lambda date: raw_filter(SimpleDate(date), lambda d: SimpleDate(parsedate(d)))

if __name__ == '__main__':
    filt = compile_filter(sys.argv[1])
    cdate = datetime.date.today()
    for j in range(100):
        v = filt(cdate)
        print(f'{cdate.isoformat()}: {v}')
        cdate += datetime.timedelta(days=1)
    #print(ast.unparse(parse_filter(sys.argv[1])))
