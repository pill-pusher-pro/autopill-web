import logging
import traceback
from pathlib import Path

# Third-party imports
from ktpanda.fileutils import load_json, save_json, save_file

from utils import BASEPATH

CONFIG = {}

CONFIG_PATH = BASEPATH / 'config.json'
PILLCHECK_PATH = BASEPATH / 'motor.asm'

DEFAULT_CONFIG = {
    "motor_speed": 255,
    "timeout":  10,
    "info_level": 2,
}

DEFAULT_MOTOR = '''\
    ;; skip delay for slide-type dispensers
    J start SLIDE

    ;; Wait half a second with servo pushed in to let any pills in tube drop
    ;; before running motors
    W .5

start:
    W .1

    ;; shake pills up, moving down briefly
    U .5

    D .3
    U .5

    D .3
    U .5

    D .3
    U .5


    ; give pills a chance to settle
    W .5 N

    ;; shake the pills to the right (or left if swapped)
    LF .3
    RB .3
    LF .3
    RB .3

    W .5 N

    ;; shake the pills to the left
    LB .3
    RB .3
    LB .3
    RB .3

    W .5 N

    SWAP

    J start
'''

def load_config():
    try:
        motor_asm = PILLCHECK_PATH.read_text(encoding='utf8', errors='ignore')
    except FileNotFoundError:
        motor_asm = DEFAULT_MOTOR

    motor_asm = motor_asm.splitlines()

    new_config = load_json(CONFIG_PATH, default=DEFAULT_CONFIG)
    new_config['motor_sequence'] = motor_asm

    CONFIG.update(new_config)

def save_config():
    config = dict(CONFIG)
    motor_asm = config.pop('motor_sequence', [])
    motor_asm = '\n'.join(motor_asm) + '\n'

    save_file(PILLCHECK_PATH, motor_asm)
    save_json(CONFIG_PATH, config)
