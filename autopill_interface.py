import sys
import os
import re
import time
import threading
import subprocess
import logging
import select

from collections import deque

import serial

from config import CONFIG

log = logging.getLogger(__name__)

DISPENSING_STATE = {
    0: 'IDLE',
    1: 'PILLCHECK',
    2: 'DISPENSE',
    3: 'RETURN_WAIT',
    4: 'RETURN'
}

DISPENSING_RESULT = {
    0: 'UNKNOWN',
    1: 'INPROGRESS',
    2: 'COMPLETE',
    3: 'CANCELLED',
    4: 'FAIL_EMPTY',
    5: 'FAIL_STUCK',
}

# I_RUN flags

M_LF = 0x48
M_LB = 0x44
M_RF = 0x42
M_RB = 0x41

M_UP = M_LB | M_RF
M_DN = M_LF | M_RB
M_LEFT = M_LF | M_RF
M_RIGHT = M_LB | M_RB

I_RUN    = 0x40
I_BR     = 0x80
I_MSWAP  = 0x01

# Slow the motors for this step
M_SLOW = 0x10

# For pincher dispensers, return the servo to neutral for this step (reduces power usage
# from servo fighting spring)
M_SVN = 0x20

def cmd_motor(base):
    def r(idx, argv, labels):
        flags = base

        if '.' in argv[1]:
            time = int(float(argv[1]) * 100)
        else:
            time = int(argv[1]) // 10

        for arg in argv[2:]:
            if arg == 's':
                flags |= M_SLOW
            elif arg == 'n':
                flags |= M_SVN
        return flags, time
    return r

def cmd_branch(idx, argv, labels):
    cond = argv[2] if len(argv) > 2 else 'a'

    if cond == 'a':
        cc = 0
    elif cond == 'slide':
        cc = 0x21
    elif cond in {'pinch', 'spring'}:
        cc = 0x01
    else:
        raise ValueError(f'Invalid condition: {cond}')

    pos = argv[1]
    if pos in labels:
        pos = labels[pos]
    elif pos[0] in '-+':
        pos = idx + int(pos)
    else:
        pos = int(pos)

    return I_BR | cc, pos

def cmd_misc(code):
    def r(idx, argv, labels):
        arg = argv[1] if len(argv) > 1 else '0'
        return code, int(arg)
    return r

INSTRUCTIONS = {
    'w': cmd_motor(I_RUN),

    'u': cmd_motor(M_UP),
    'd': cmd_motor(M_DN),
    'l': cmd_motor(M_LEFT),
    'r': cmd_motor(M_RIGHT),

    'lf': cmd_motor(M_LF),
    'lb': cmd_motor(M_LB),
    'rf': cmd_motor(M_RF),
    'rb': cmd_motor(M_RB),

    'j': cmd_branch,

    'swap': cmd_misc(I_MSWAP),
}

def parse_instruction(idx, instr, labels):
    argv = instr.lower().split()
    try:
        cmdf = INSTRUCTIONS[argv[0]]
    except KeyError:
        log.error(f'Invalid instruction: {instr!r}')
        return 0, 50

    try:
        return cmdf(idx, argv, labels)
    except ValueError as e:
        log.error(f'Invalid argument for {instr!r}: {e}')
    except Exception:
        log.exception(f'Error parsing {instr!r}')

    return 0, 50

class UpdateError(Exception):
    pass

class BaseRequest:
    def __init__(self, lock, on_complete):
        self._lock = lock
        self._cond = None
        self.result = None
        self.message = None
        self.on_complete = on_complete
        self.info = []
        self.id = None
        self.change_count = 0
        self.start = time.time()

    def _have_result(self, change_count=None):
        if self.result is not None:
            return True

        if change_count is not None and self.change_count != change_count:
            return True

        return False

    def wait(self, timeout=30, change_count=None):
        with self._lock:
            if self._have_result(change_count):
                return True

            if self._cond is None:
                self._cond = threading.Condition(self._lock)

            endtime = time.time() + timeout
            while not self._have_result(change_count):
                rtime = endtime - time.time()
                if rtime < 0:
                    return False
                self._cond.wait(rtime)

            return True

    def _notify_change(self):
        self.change_count += 1
        if self._cond is not None:
            self._cond.notify_all()

    def _set_result(self, result, message):
        if self.result is not None:
            return

        self.result = result
        self.message = message
        if self._cond is not None:
            self._cond.notify_all()

    def set_result(self, result, message):
        with self._lock:
            self._set_result(result, message)

        if self.on_complete:
            self.on_complete(self)

    def tojson(self):
        return {
            'id': self.id,
            'complete': self.result is not None,
            'result': self.result,
            'message': self.message,
            'info': self.info,
            'change_count': self.change_count,
        }

class CommandRequest(BaseRequest):
    def __init__(self, command, lock, on_complete):
        super().__init__(lock, on_complete)
        self.command = command

class DispenseRequest(BaseRequest):
    def __init__(self, unit, count, lock, on_complete):
        super().__init__(lock, on_complete)
        self.unit = unit
        self.count = count
        self.pill_count = 0

    def tojson(self):
        r = super().tojson()
        r['pill_count'] = self.pill_count
        return r

class AutopillInterface(threading.Thread):
    instance = None

    def __init__(self, port):
        super().__init__(daemon=True)
        self._lock = threading.Lock()
        self._status_change = threading.Condition(self._lock)
        self.command_queue = deque()
        self.dispense_queue = deque()
        self.active_dispense_req = None

        self.completion_queue = []

        self.port = port
        self.serial = None

        self.updating = False

        self.status_count = 0
        self.dispensing_state = 'UNKNOWN'
        self.dispensing_result = 'UNKNOWN'
        self.dispense_remain = 0
        self.dispense_count = 0
        self.pill_in_chamber = False
        self.pill_in_chamber_stable = False

        self.next_id = 1
        self.outstanding_requests = {}

    def open(self):
        ser = serial.Serial(self.port, baudrate=115200, timeout=10)
        self.serial = ser
        return ser

    def close(self):
        ser = self.serial
        if ser is not None:
            self.serial = None
            try:
                ser.close()
            except Exception:
                pass

    def update_firmware(self, script):
        with self._lock:
            if self.updating:
                raise UpdateError('update already in progress')

            if self.command_queue:
                raise UpdateError('commands in progress')
                log.error()

            if self.dispense_queue or self.active_dispense_req:
                raise UpdateError('dispense in progress')

            self.updating = True
            self.close()

        try:
            log.info('update_firmware: beginning update')
            subprocess.run([script, self.port], check=True)
            log.info('update_firmware: update complete')
        finally:
            with self._lock:
                self.updating = False

    def get_status(self, prev_count=None, timeout=0):
        with self._lock:
            if self.status_count == prev_count and timeout:
                self._status_change.wait(timeout)

            return {
                'status_count': self.status_count,
                'result': self.dispensing_result,
                'state': self.dispensing_state,
                'count': self.dispense_count,
                'remain': self.dispense_remain,
                'pill_in_chamber': self.pill_in_chamber,
                'pill_in_chamber_stable': self.pill_in_chamber_stable,
            }

    def _send_next_command(self):
        cmd = self.command_queue[0].command
        log.info(f'send command: {cmd}')
        self.serial.write(f'[{cmd}]'.encode('utf8'))

    def _check_dispense_queue(self):
        if (self.dispense_queue and
            not self.active_dispense_req and
            self.dispensing_result != 'INPROGRESS'):
            next_req = self.dispense_queue.popleft()
            self.active_dispense_req = next_req
            self._command(f'P{next_req.unit} {next_req.count}')
            return True
        return False

    def _set_result(self, req, result, message):
        req._set_result(result, message)
        if req.on_complete:
            self.completion_queue.append(req)

    def _assign_id(self, req):
        req_id = req.id = self.next_id
        self.next_id = req_id + 1
        self.outstanding_requests[req_id] = req

    def _command(self, cmd, complete=None):
        req = CommandRequest(cmd, self._lock, complete)
        self._assign_id(req)

        if self.serial is None:
            self._set_result(req, False, 'no connection')
            return req

        self.command_queue.append(req)

        if len(self.command_queue) == 1:
            self._send_next_command()

        return req

    def command(self, cmd, complete=None):
        with self._lock:
            return self._command(cmd, complete)

    def queue_dispense(self, unit, count, complete=None):
        req = DispenseRequest(unit, count, self._lock, complete)
        with self._lock:
            self._assign_id(req)
            if self.serial is None:
                self._set_result(req, False, 'no connection')
                return req

            self.dispense_queue.append(req)
            self._check_dispense_queue()
            return req


    def set_motor_speed(self, speed):
        CONFIG['motor_speed'] = speed
        self.command(f's{speed}')

    def set_dispense_timeout(self, timeout):
        CONFIG['timeout'] = timeout
        self.command(f'T{timeout}')

    def set_info_level(self, level):
        CONFIG['info_level'] = level
        self.command(f'i{level}')

    def set_pillcheck(self, seq):
        CONFIG['motor_sequence'] = seq

        instrs = []
        labels = {}

        for line in seq:
            # remove comment
            line = line.lower().partition(';')[0].strip()

            if m := re.match(r'^(\w+):(.*)', line):
                labels[m.group(1)] = len(instrs)
                line = m.group(2).strip()

            if line:
                instrs.append(line)

        for i, instr in enumerate(instrs):
            cmd, arg = parse_instruction(i, instr, labels)
            self.command(f'S{i} {cmd} {arg}')

        # clear the rest of the sequence
        self.command(f'S{len(instrs)} E')

    def send_config(self):
        self.set_info_level(CONFIG['info_level'])
        self.set_motor_speed(CONFIG['motor_speed'])
        self.set_dispense_timeout(CONFIG['timeout'])
        self.set_pillcheck(CONFIG['motor_sequence'])

    def sync_time(self):
        def complete(req):
            if req.result:
                dt = [int(v) for v in req.message.split()]
                year, month, day, hour, min, sec = dt[:6]
                if year > 2000 and 1 <= month <= 12 and 1 <= day <= 31:
                    new_time = f'{year:04d}-{month:02d}-{day:02d}T{hour:02d}:{min:02d}:{sec:02d}'
                    log.info(f'new time = {new_time}')
                    subprocess.call(['sudo', 'date', '-u', '-s', new_time])

        self.command('r', complete=complete)

    def get_request(self, id):
        return self.outstanding_requests.get(id)

    def clear_expired_requests(self):
        mintime = time.time() - 7200
        done = None
        for req in self.outstanding_requests.values():
            if req.start < mintime:
                if done is None:
                    done = [req.id]
                else:
                    done.append(req.id)
        if done is not None:
            for id in done:
                del self.outstanding_requests[id]

    def _clear_command_queue(self):
        with self._lock:
            queue = list(self.command_queue)
            self.command_queue.clear()

            for res in queue:
                res._set_result(False, 'connection error')
                if res.on_complete:
                    self.completion_queue.append(res)

    def _find_command(self, line):
        lost = []
        cmdchar = line[1]
        with self._lock:
            while True:
                try:
                    res = self.command_queue.popleft()
                except IndexError:
                    log.error(f'Could not find command for result line {line!r}')
                    return None
                if res.command[0] == cmdchar:
                    if self.command_queue:
                        self._send_next_command()
                    return res
                res._set_result(False, 'command lost')
                if res.on_complete:
                    self.completion_queue.append(res)
                log.error(f'command lost: {res.command}')

    def _handle_line(self, line):
        ch = line[0]
        if ch == ':':
            data = line.split(':')
            with self._lock:
                self.dispensing_result = DISPENSING_RESULT.get(int(data[1]), 'UNKNOWN')
                self.dispensing_state = DISPENSING_STATE.get(int(data[2]), 'UNKNOWN')
                self.dispense_remain = int(data[3])
                self.dispense_count = int(data[4])
                pic = int(data[5])
                self.pill_in_chamber = bool(pic & 1)
                self.pill_in_chamber_stable = bool(pic & 2)

                req = self.active_dispense_req
                if req and self.dispense_count != req.pill_count:
                    req.pill_count = self.dispense_count
                    req._notify_change()

                if self.dispensing_result != 'INPROGRESS':
                    if req:
                        req.pill_count = self.dispense_count
                        self._set_result(req, self.dispensing_result == 'COMPLETE', self.dispensing_result)
                    self.active_dispense_req = None
                    if self.dispensing_result == 'CANCELLED':
                        for req in self.dispense_queue:
                            log.info(f'cancel outstanding request {req.id}')
                            self._set_result(req, False, self.dispensing_result)
                        self.dispense_queue.clear()
                        if req:
                            self._command('U')
                    else:
                        if req and not self._check_dispense_queue():
                            self._command('U')

                self.status_count += 1
                self._status_change.notify_all()

        elif ch == '>':
            log.info(line[1:])
            # arduino was reset, resend config
            if line == '>autopill initialized':
                self._clear_command_queue()
                self.send_config()
                self.sync_time()

        elif ch == '/':
            with self._lock:
                try:
                    res = self.command_queue[0]
                except IndexError:
                    return
                res.info.append(line[1:])

        elif ch in {'*', '!'}:
            res = self._find_command(line)
            if res:
                res.set_result(ch == '*', line[3:])

        else:
            log.info(f'unrecognized: {line!r}')

    def _run_reader(self, serial):
        if os.name == 'posix':
            fd = serial.fileno()
            def read():
                r, w, e = select.select((fd,), (), (), 5)
                if fd in r:
                    if serial is not self.serial:
                        raise EOFError()
                    data = os.read(fd, 256)
                    if not data:
                        raise EOFError()
                    return data
        else:
            def read():
                return serial.read(serial.in_waiting or 1)

        linebuf = b''
        while True:
            with self._lock:
                compq = self.completion_queue
                if compq:
                    self.completion_queue = []
                else:
                    compq = None

            if compq is not None:
                for cmd in compq:
                    cmd.on_complete(cmd)

            data = read()
            if serial is not self.serial:
                return
            if not data:
                continue

            linebuf += data
            lines = linebuf.split(b'\n')
            linebuf = lines.pop()
            for line in lines:
                line = line.rstrip(b'\r').decode('utf8', 'replace')
                #log.info(f'recv line {line!r}')
                try:
                    if line:
                        self._handle_line(line)
                except Exception:
                    self._clear_command_queue()
                    log.exception(f'Error processing line: {line!r}')

    def run(self):
        last_open_error = None

        while True:
            if self.updating:
                time.sleep(1)
                continue

            try:
                with self._lock:
                    serial = self.open()
            except Exception as e:
                errmsg = str(e)
                if errmsg != last_open_error:
                    log.error(f'Error opening serial port: {errmsg}')
                    last_open_error = errmsg
                time.sleep(1)
                continue

            last_open_error = None
            log.info(f'Port {self.port} opened')
            try:
                self._run_reader(serial)
            except Exception:
                with self._lock:
                    # Did another thread close the port to update the firmware?
                    if serial is not self.serial:
                        continue
                    self.close()
                log.exception('Error reading from serial port')
                time.sleep(1)
