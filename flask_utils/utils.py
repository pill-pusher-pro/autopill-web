import time
import os
import re
import json
import socket
import logging
import colorsys
import datetime
import builtins
import traceback
from contextlib import contextmanager

# Third-party imports
import flask

EPOCH = datetime.datetime(1970, 1, 1, 0, 0, 0)

log = logging.getLogger(__name__)

maintenance_tasks = []

def getbool(text, default=None):
    text = str(text).strip().lower()
    if text in ('yes', 'true', '1', 'on'):
        return True
    if text in ('no', 'false', '0', 'off'):
        return False
    return default

def get_request_int(key, default=None):
    val = flask.request.args.get(key) or None
    if val is not None:
        return int(val)
    return default

CONTENT_TYPE = {
    'html': 'text/html; charset=utf-8',
    'htm': 'text/html; charset=utf-8',
    'js': 'text/javascript',
    'png': 'image/png',
    'txt': 'text/plain',
    'css': 'text/css',
    'mp4': 'video/mp4',
    'zip': 'application/zip',
}

def maintenance_task(f):
    maintenance_tasks.append(f)

def run_maintenance_tasks():
    for f in maintenance_tasks:
        try:
            f()
        except Exception:
            log.exception('Error running maintenance task {f.__name__}')

def response_for_file(data, filename):
    return flask.Response(data, content_type=CONTENT_TYPE.get(filename.split('.')[-1], 'application/octet-stream'))

def tojson(val):
    if flask.request.args.get('comp'):
        data = json.dumps(val, separators=(',',':'))
    else:
        data = json.dumps(val, indent=True)
    return flask.Response(data, content_type='application/json')

def read_json(path, default=None):
    try:
        with open(path, 'r', encoding='utf8') as fp:
            return json.load(fp)
    except FileNotFoundError:
        return default

def write_json(path, data):
    with open(path + '~', 'w', encoding='utf8') as fp:
        json.dump(data, fp)
    os.replace(path + '~', path)

def try_unlink(path):
    try:
        os.unlink(path)
    except OSError:
        pass

def namefunc(name):
    '''Decorator to rename a function. Necessary for templates because
    Flask won't allow two endpoints with the same function name.'''

    def rf(f):
        f.__name__ = name
        return f
    return rf

def exception_to_json(e):
    return {
        'type': type(e).__name__,
        'args': e.args,
        'message': str(e),
        'traceback': traceback.format_exc()
    }
