'''
systemdservice
==============

Handles installing and running as a SystemD service.
'''

import sys
import os
import traceback
import systemd

from . import service

class FlaskService(service.FlaskServiceBase):
    pass

service.FlaskService = FlaskService
