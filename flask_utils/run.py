import sys
import os
import re
import time
import json
import argparse
import traceback
import logging
import logging.handlers
import socket
import subprocess
import threading
from pathlib import Path
from collections import defaultdict

from socketserver import ThreadingMixIn
from logging.config import dictConfig
from werkzeug.serving import BaseWSGIServer, ThreadedWSGIServer

from ktpanda import threadpool

from . import service
from . import utils

if os.name == 'nt':
    from . import uac_elevate
else:
    uac_elevate = None

is_frozen = hasattr(sys, 'frozen')

RX_FILTER = re.compile(r'(GET|POST|PUT|DELETE) /([^\s/]*)')

def get_main_path(module_name='__main__'):
    if is_frozen:
        return Path(sys.executable).absolute().parent
    else:
        try:
            return Path(sys.modules[module_name].__file__).parent.absolute()
        except Exception:
            return Path('.').absolute()

class RequestLogFilter(logging.Filter):
    def __init__(self, filters):
        self.filters = filters

    def filter(self, rec):
        # This is a hack to prevent requests from spamming the logs, especially when logs
        # are being requested
        m = RX_FILTER.search(rec.getMessage())
        if m:
            if m.group(2) in self.filters[m.group(1)]:
                return False
        return True

class ThreadpoolMixIn(ThreadingMixIn):
    def process_request(self, request, client_address):
        self.__threadpool.run(self.process_request_thread, request, client_address)

    def set_thread_pool(self, pool, reap=False):
        self.__threadpool = pool
        self.__reap = reap

    def service_actions(self):
        super().service_actions()
        if self.__reap:
            self.__threadpool.reap()
            utils.run_maintenance_tasks()

class ThreadpoolWSGIServer(ThreadpoolMixIn, BaseWSGIServer):
    multithread = True
    daemon_threads = True

class FlaskRunner(service.FlaskService):
    want_elevate = False
    main_script = None
    ports = ()

    def __init__(self):
        super().__init__(self)
        self.socket_servers = ()
        self.filters = defaultdict(set)
        self.filters['GET'].update({'', 'livelog', 'static', 'favicon.ico', 'robots.txt'})

        if self.main_script is None:
            try:
                self.main_script = Path(sys.modules['__main__'].__file__)
            except AttributeError:
                pass

    def log_filter(self, method, path):
        self.filters[method.upper()].add(path)

    def add_args(self, p):
        p.add_argument('-P', '--port', default='8082', help=f'Port to listen on: (default: {",".join(map(str, self.ports))})')
        p.add_argument('--no-elevate', action='store_true', help=('Do not attempt to elevate' if uac_elevate else argparse.SUPPRESS))

        if service.FlaskService.can_install_service():
            p.add_argument('--install', action='store_true',
                               help=f'Install {self.appname} as a service.')
            p.add_argument('--uninstall', action='store_true',
                               help='Uninstall the service.')

            p.add_argument('--start', action='store_true', help='Start the service.')
            p.add_argument('--stop', action='store_true', help='Stop the service, but don\'t uninstall it.')
            p.add_argument('--query-installed', action='store_true',
                            help='Exits with code 0 if the service is installed, '
                            'or code 1 if it is not.')
        else:
            # Debug mode depends on .py files being present, doesn't work when built as EXE
            p.add_argument('-D', '--debug', action='store_true', help='Run flask in debug mode')

        # This argument is set up to be passed when launched as a service
        p.add_argument('--run-service', action='store_true', help=argparse.SUPPRESS)

        # This argument is passed to the child process that we launch in a loop when --debug is passed
        p.add_argument('--run-debug', type=int, help=argparse.SUPPRESS)

        return p

    def check_args(self, args):
        debug = not service.is_frozen and args.debug

        if self.want_elevate and uac_elevate and not args.no_elevate:
            if not uac_elevate.is_admin():
                print('Relaunching with admin privileges (pass --no-elevate to disable)')
                uac_elevate.elevate(False, sys.argv[1:] + ['--no-elevate'])
                return True

        if debug and not args.run_debug:
            runner_args = None
            if service.is_frozen:
                runner_args = [sys.executable]
            elif self.main_script is not None:
                runner_args = [sys.executable, self.main_script]

            if runner_args is not None:
                runner_args.append('--run-debug')
                runner_args.append(str(os.getpid()))
                runner_args.extend(sys.argv[1:])
                while True:
                    proc = subprocess.run(runner_args)
                    if proc.returncode != 0:
                        print(f'runner exited with code {proc.returncode}')
                        time.sleep(2)
                    else:
                        print(f'runner restarting...')

                return True

        if args.run_service:
            self.run_service()
            return True

        if service.is_frozen:
            if args.query_installed:
                if self.query_service_installed():
                    sys.exit(0)
                else:
                    sys.exit(1)

            try:
                if args.install:
                    self.install_service()
                    if args.start:
                        self.start_service()
                    return True

                if args.uninstall:
                    self.stop_service()
                    self.uninstall_service()
                    return True

                if args.start or args.stop:
                    if args.stop:
                        self.stop_service()
                    if args.start:
                        self.start_service()
                    return True

            except EnvironmentError as e:
                print(str(e), file=sys.stderr)
                sys.exit(1)

        if args.debug:
            os.environ['FLASK_ENV'] = 'development'
            os.environ['FLASK_TESTING'] = 'true'
            os.environ['FLASK_DEBUG'] = 'true'
            os.environ['FLASK_PROPAGATE_EXCEPTIONS'] = 'true'
        else:
            os.environ['FLASK_ENV'] = 'production'
            os.environ['FLASK_TESTING'] = 'false'
            os.environ['FLASK_DEBUG'] = 'false'
            os.environ['FLASK_PROPAGATE_EXCEPTIONS'] = 'false'

        return False

    def config_log(self):
        log_config = {
                'version': 1,
                'formatters': {
                    'console': {
                        'format': '[%(asctime)s] %(levelname)s %(message)s',
                    },
                    'live': {
                        'format': '%(message)s',
                    },
                    'logfile': {
                        'format': '[%(asctime)s] %(levelname)s: %(message)s',
                    }
                },

                'handlers': {
                    'wsgi': {
                        'class': 'logging.StreamHandler',
                        'stream': 'ext://flask.logging.wsgi_errors_stream',
                        'formatter': 'console'
                    },
                    'live': {
                        'class': 'flask_utils.livelog.LiveLogger',
                        'formatter': 'live'
                    }
                },
               'root': {
                    'level': 'INFO',
                    'handlers': ['wsgi', 'live']
                }
            }

        dictConfig(log_config)

        # Disable request logging
        wzlog = logging.getLogger('werkzeug')
        wzlog.addFilter(RequestLogFilter(self.filters))

    def on_shutdown(self):
        pass

    def shutdown(self):
        for svr in self.socket_servers:
            svr.shutdown()

    def run_flask(self, args, app):
        log = logging.getLogger('main')

        ports = [int(v.strip()) for v in args.port.split(',')]
        self.ports = ports

        if args.run_debug:
            monitor_pid = args.run_debug
            if os.name == 'nt':
                from winutils import process_exists
            elif os.name == 'posix':
                def process_exists(pid):
                    try:
                        os.kill(pid, 0)
                        return True
                    except Exception:
                        return False
            else:
                monitor_pid = 0

            main_path = self.main_script.absolute().parent
            reload_files = []
            for mod in sys.modules.values():
                try:
                    path = mod.__file__
                     # built-in modules might have __file__ set to None
                    if isinstance(path, str) and path.endswith('.py'):
                        path = Path(path).absolute()

                        # If the file is within the base directory, monitor it for changes
                        if path.name and (path.parent == main_path or path.parent.parent == main_path):
                            mtime = path.stat().st_mtime
                            reload_files.append((path, mtime))

                except (AttributeError, OSError):
                    pass

            reload_files.sort()
            log.info(f'Monitoring files for changes: {" ".join(path.name for path, mtime in reload_files)}')
            def check_files():
                while True:
                    # If monitor process exits, then we need to exit as well
                    if monitor_pid:
                        res = process_exists(monitor_pid)

                        if not res:
                            log.info(f'Monitor process has exited, stopping agent')
                            os._exit(1)

                    for path, orig_mtime in reload_files:
                        try:
                            new_mtime = path.stat().st_mtime
                        except OSError:
                            new_mtime = 0
                        if new_mtime != orig_mtime:
                            log.info(f'Change detected in {path}, reloading')
                            os._exit(0)
                    time.sleep(1)

            threading.Thread(target=check_files, args=(), daemon=True).start()

        try:
            if args.debug:
                from werkzeug.debug import DebuggedApplication
                app = DebuggedApplication(app, False)

            pool = threadpool.ThreadPool(maxthreads=30, minthreads=5, maxbacklog=100)
            threadpool.set_main_pool(pool)
            servers = []
            for port in ports:
                if os.name == 'nt':
                    addrs = ('0.0.0.0', '::')
                else:
                    addrs = ('::',)
                for addr in addrs:
                    srv = ThreadpoolWSGIServer(
                        host=addr,
                        port=port,
                        app=app
                    )
                    srv.set_thread_pool(pool)
                    servers.append(srv)

            self.socket_servers = servers
            self.report_running()

            # Tell the first server to do the threadpool reaping in service_actions
            servers[0].set_thread_pool(pool, True)

            for srv in servers[1:]:
                threading.Thread(target=srv.serve_forever, daemon=True).start()
            servers[0].serve_forever()
        finally:
            self.on_shutdown()
