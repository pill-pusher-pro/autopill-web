'''
winutils
==============

Contains various Windows API helpers for accessing the registry, managing processes, etc.
'''

import win32con as wc
import win32api
import win32file
import win32event
import win32process
import win32security
import pywintypes
import traceback
import ctypes
import time
from contextlib import contextmanager
from os.path import basename
from utils import Closable

class RegistryKey(Closable):
    '''Represents an open handle to a Windows Registry key.'''

    def __init__(self, root, path, w64=True, write=False, create=False):
        '''Open the registry key starting at `root` (such as win32con.HKEY_LOCAL_MACHINE
        or another open registry key), following the subkey `path`. `w64` is a boolean
        specifying whether the registry should be viewed as a 64-bit program would (True)
        or a 32-bit program (False). '''

        self.root = root
        self.path = path
        self.w64 = w64
        self.handle = None

        flags = wc.KEY_WOW64_64KEY if w64 else wc.KEY_WOW64_32KEY
        if write or create:
            flags |= wc.KEY_WRITE | wc.KEY_SET_VALUE

        if create:
            self.handle, res = win32api.RegCreateKeyEx(root, path, wc.KEY_READ | flags, None, 0, None, None)
        else:
            self.handle = win32api.RegOpenKeyEx(root, path, 0, wc.KEY_READ | flags)

    def subkey(self, path, write=False):
        return RegistryKey(self.handle, path, self.w64, write=write)

    def read_value(self, name):
        return win32api.RegQueryValueEx(self.handle, name)

    def set_value(self, name, value, type=None):
        if type is None:
            if isinstance(value, int):
                type = wc.REG_DWORD
            elif isinstance(value, bytes):
                type = wc.REG_BINARY
            else:
                type = wc.REG_SZ
        win32api.RegSetValueEx(self.handle, name, 0, type, value)

    def subkey_names(self):
        try:
            idx = 0
            while True:
                yield win32api.RegEnumKey(self.handle, idx)
                idx += 1
        except pywintypes.error as e:
            if e.args[0] != 259:
                raise

    def subkeys(self, write=False):
        for name in self.subkey_names():
            yield self.subkey(name, write)

    def close(self):
        if self.handle is not None:
            win32api.RegCloseKey(self.handle)
        self.handle = None

def enum_uninstall_uuids():
    '''List all registry keys for uninstall information for installed programs, both 32
    and 64-bit. Yields tuples of (key, uuid) where `key` is a RegistryKey instance
    representing the parent key and `uuid` is the name of the subkey. Use
    `key.subkey(uuid)` to access the subkey.'''

    for w64 in (False, True):
        path = r'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall'
        with RegistryKey(wc.HKEY_LOCAL_MACHINE, path, w64) as key:
            for uuid in sorted(key.subkey_names()):
                if uuid.startswith('{') and uuid.endswith('}'):
                    yield key, uuid


def enum_uninstall_info():
    for key, uuid in enum_uninstall_uuids():
        try:
            with key.subkey(uuid) as subkey:
                displayname, typ = subkey.read_value('DisplayName')
                source, typ = subkey.read_value('InstallSource')
                yield subkey, uuid, displayname, source
        except pywintypes.error:
            continue

def visit_process_by_name(func):
    wtextstr = ctypes.c_wchar * 1024
    try:
        f = GetProcessImageFileName = ctypes.windll.kernel32.GetProcessImageFileNameW
    except Exception:
        f = GetProcessImageFileName = ctypes.windll.psapi.GetProcessImageFileNameW
    buf = wtextstr()
    pids = win32process.EnumProcesses()
    for pid in pids:
        try:
            handle = win32api.OpenProcess(wc.PROCESS_QUERY_INFORMATION | wc.PROCESS_TERMINATE | wc.SYNCHRONIZE, False, pid)
        except pywintypes.error:
            continue

        try:
            res = GetProcessImageFileName(int(handle), buf, len(buf))
            if res == 0:
                continue
            filename = buf.value
            func(pid, filename, handle)
        except Exception:
            traceback.print_exc()
        finally:
            win32api.CloseHandle(handle)

def wait_and_kill(procname, endtime=0):
    procname = procname.lower()

    def visit(pid, filename, handle):
        curname = basename(filename).lower()
        if curname == procname:
            while True:
                remain_time = endtime - time.time()
                wait_time = max(0, min(500, int(1000 * remain_time)))
                wait_result = win32event.WaitForSingleObject(handle, wait_time)
                if wait_result == wc.WAIT_OBJECT_0:
                    return

                if remain_time <= 0:
                    break

            win32process.TerminateProcess(handle, 0)

    visit_process_by_name(visit)

def terminate_pid(pid):
    handle = None
    try:
        handle = win32api.OpenProcess(wc.PROCESS_QUERY_INFORMATION | wc.PROCESS_TERMINATE | wc.SYNCHRONIZE, False, pid)
        win32process.TerminateProcess(handle, 0)
    finally:
        if handle:
            win32api.CloseHandle(handle)

def process_exists(pid):
    handle = None
    try:
        handle = win32api.OpenProcess(wc.PROCESS_QUERY_INFORMATION, False, pid)
        return True
    finally:
        if handle:
            win32api.CloseHandle(handle)
    return False

def give_full_access(path):
    '''Give "Everyone" full access to a file or directory.'''

    sid_world = win32security.CreateWellKnownSid(win32security.WinWorldSid, None)
    flags = win32security.OBJECT_INHERIT_ACE | win32security.CONTAINER_INHERIT_ACE

    desc = win32security.GetNamedSecurityInfo(path, win32security.SE_FILE_OBJECT, win32security.DACL_SECURITY_INFORMATION)
    dacl = desc.GetSecurityDescriptorDacl()
    dacl.AddAccessAllowedAceEx(win32security.ACL_REVISION_DS, flags, win32file.FILE_ALL_ACCESS, sid_world)

    win32security.SetNamedSecurityInfo(path, win32security.SE_FILE_OBJECT, win32security.DACL_SECURITY_INFORMATION, None, None, dacl, None)
