'''
livelog
=======

Captures logger output into a list that can be queried
'''

import logging
import re

_loglines = []

# Werkzeug tries to get all fancy with its output. Strip out any terminal control codes.
RX_UNSTYLE = re.compile(r'\033\[[;0-9]*[a-zA-Z]')

class LiveLogger(logging.Handler):
    def __init__(self):
        super().__init__()
        self._max_lines = 10000
        self._purge_lines = 1000

    def emit(self, record):
        msg = RX_UNSTYLE.sub('', self.format(record))
        mstime = int(record.created * 1000)
        ll = _loglines
        try:
            lasttime = ll[-1]['time']
            if mstime <= lasttime:
                mstime = lasttime + 1
        except IndexError:
            pass

        ll.append({
            'time': mstime,
            'name': record.name,
            'level': record.levelname,
            'msg': msg
        })
        if len(ll) >= self._max_lines:
            del ll[:self._purge_lines]

def get_lines_after(mstime=0):
    '''Get all log lines whose timestamp is greater than `mstime`'''

    # It's possible the beginning of the list could shrink due to purging. Use a negative
    # index so that our actual position is preserved.

    rval = []
    ll = _loglines
    idx = -1
    try:
        while True:
            line = ll[idx]
            if line['time'] <= mstime:
                break
            rval.append(line)
            idx -= 1
    except IndexError:
        pass

    rval.reverse()
    return rval
