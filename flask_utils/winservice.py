'''
winservice
==========

Handles installing and running as a Windows service.
'''
import sys
import os
import traceback
import win32serviceutil
import win32service
import pywintypes
import threading

from . import service

class FlaskService(win32serviceutil.ServiceFramework, service.FlaskServiceBase):
    # override in subclass
    _svc_name_ = None
    _svc_display_name_ = None
    _svc_description_ = None
    shutdown_wait_timeout = 5
    
    _exe_args_ = '--run-service'
    main_args = ['--no-elevate']
    install_args = ['--startup', 'auto', 'install']
    server = None
    
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        if not self._svc_name_:
            raise NotImplementedError('Child class must specify _svc_name_')

        self._main_thread = None
        self._lock = threading.Lock()
        self._exit_cond = threading.Condition(self._lock)
        self._ready = False
        self._exit = False

    def SvcRun(self):
        '''Start and run the service.'''
        import servicemanager
        try:
            reported_running = False
            servicemanager.LogInfoMsg('%s - Starting (%r)' % (self._svc_name_, sys.executable))
            with self._lock:
                self._main_thread = threading.Thread(target=self._run_main_thread, daemon=True)
                self._main_thread.start()

                while not self._exit:
                    self._exit_cond.wait(5)
                    if not reported_running:
                        if self._ready:
                            self.ReportServiceStatus(win32service.SERVICE_RUNNING)
                            reported_running = True

        except Exception:
            servicemanager.LogErrorMsg(traceback.format_exc())
        finally:
            self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
            self._main_thread.join(self.shutdown_wait_timeout)

    def SvcStop(self):
         '''Stop the service.'''
         import servicemanager
         servicemanager.LogInfoMsg('%s - Shutting down' % self._svc_name_)
         self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)

         try:
             self.shutdown()
        except Exception:
            servicemanager.LogErrorMsg(traceback.format_exc())

         with self._lock:
             self._exit = True
             self._exit_cond.notify_all()

    @classmethod
    def can_install_service(cls):
        return service.is_frozen
    
    @classmethod
    def service_action(cls, args):
        return win32serviceutil.HandleCommandLine(cls, argv=[''] + list(args))

    @classmethod
    def install_service(cls):
        return cls.service_action(cls.install_args)

    @classmethod
    def uninstall_service(cls):
        return cls.service_action(['remove'])

    @classmethod
    def start_service(cls):
        return cls.service_action(['start'])

    @classmethod
    def stop_service(cls):
        return cls.service_action(['stop'])

    @classmethod
    def run_service(cls):
        import servicemanager
        servicemanager.PrepareToHostSingle(cls)
        res = servicemanager.StartServiceCtrlDispatcher()

    @classmethod
    def query_service_installed(cls):
        hscm = win32service.OpenSCManager(None, None, win32service.SC_MANAGER_ALL_ACCESS)
        try:
            try:
                hs = win32serviceutil.SmartOpenService(hscm, cls._svc_name_, win32service.SERVICE_ALL_ACCESS)
                win32service.CloseServiceHandle(hs)
                return True
            except pywintypes.error:
                return False
        finally:
            win32service.CloseServiceHandle(hscm)

    def _run_main_thread(self):
        try:
            self.run_main()
        except Exception:
            import servicemanager
            servicemanager.LogErrorMsg(traceback.format_exc())
            
    def report_running(self):
        with self._lock:
            # Notify the main service thread that we're ready
            self._ready = True
            self._exit_cond.notify_all()
            
    def shutdown(self):
        pass
    
    def run_main(self):
        pass

service.FlaskService = FlaskService
