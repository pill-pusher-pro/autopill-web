import sys
import platform
import os
import threading

is_frozen = hasattr(sys, 'frozen')

class FlaskServiceBase:
    # override in subclass
    _svc_name_ = None
    _svc_display_name_ = None
    _svc_description_ = None

    _exe_args_ = '--run-service'
    shutdown_wait_timeout = 5

    def __init__(self, *a, **kw):
        if not self._svc_name_:
            raise NotImplementedError('Child class must specify _svc_name_')

        self._main_thread = None
        self._lock = threading.Lock()
        self._exit_cond = threading.Condition(self._lock)
        self._ready = False
        self._exit = False

    @classmethod
    def can_install_service(cls):
        return False

    @classmethod
    def service_action(cls, args):
        raise NotImplementedError

    @classmethod
    def install_service(cls):
        raise NotImplementedError

    @classmethod
    def uninstall_service(cls):
        raise NotImplementedError

    @classmethod
    def start_service(cls):
        raise NotImplementedError

    @classmethod
    def stop_service(cls):
        raise NotImplementedError

    @classmethod
    def run_service(cls):
        raise NotImplementedError

    @classmethod
    def query_service_installed(cls):
        raise NotImplementedError

    def _run_main_thread(self):
        try:
            self.run_main()
        except Exception:
            traceback.print_exc()

    def report_running(self):
        pass

    def shutdown(self):
        pass

    def run_main(self):
        pass

class FlaskService(FlaskServiceBase):
    pass

initname = 'unknown'
if os.name == 'nt':
    osname = 'Windows'
    initname = 'windows'
    from . import winservice
else:
    from os.path import exists
    osname = platform.system()
    if osname == 'Linux':
        if exists('/usr/bin/systemctl'):
            try:
                import systemd
            except ImportError:
                pass
            else:
                from . import systemdservice

            initname = 'systemd'
