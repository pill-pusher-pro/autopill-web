'''
uac_elevate
===========

Implements functionality to run a program as Administrator, including re-launching the current
script as Administrator.

'''

import sys
import ctypes
import win32con as wc
import win32api
import win32wnet
import win32process
import pywintypes
import win32event
import win32file

from win32com.shell.shell import ShellExecuteEx, SHGetFolderPath
from win32com.shell import shellcon

from os.path import dirname, basename, join, exists

def is_admin():
    '''Returns True if the current process is running with Administrator privileges.
    '''
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except Exception:
        return False

# Implements the escaping rules detailed at
# http://blogs.msdn.com/b/oldnewthing/archive/2010/09/17/10063629.aspx
def winshellescape(v):
    return '"%s"' % '\\"'.join(c + '\\' * (len(c) - len(c.rstrip('\\'))) for c in v.split('"'))

def run_as_admin(exe, argv, wait, dir):
    cmdline = ' '.join(winshellescape(v) for v in argv)
    info = ShellExecuteEx(nShow=wc.SW_SHOWNORMAL,
                          fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
                          lpVerb='runas',
                          lpDirectory=dir,
                          lpFile=exe,
                          lpParameters=cmdline)

    proc = info['hProcess']
    if wait:
        while True:
            rv = win32event.WaitForSingleObject(proc, 1000)
            if rv != wc.WAIT_TIMEOUT:
                break
    win32file.CloseHandle(proc)

def get_universal_path(file):
    '''
    Finds the absolute UNC path of a file. If we re-run the script as Administrator, network drive
    mappings don't work.
    '''
    try:
        return win32wnet.WNetGetUniversalName(file)
    except pywintypes.error as e:
        if e.args[0] in (1200, 2250):
            # Not a mapped drive, just leave as local path
            return file
        else:
            raise

def elevate(wait=False, argv_extra=()):
    '''Re-run the current script as administrator.'''

    frozen = hasattr(sys, 'frozen')
    exe = get_universal_path(sys.executable)
    rundir = dirname(exe)

    argv = []
    if not frozen:
        script = get_universal_path(sys.modules['__main__'].__file__)
        argv.append(script)
        rundir = dirname(script)

    argv.extend(argv_extra)
    run_as_admin(exe, argv, wait, rundir)
