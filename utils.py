import time
import os
import re
import json
import socket
import logging
import colorsys
import datetime
import builtins
import traceback
from contextlib import contextmanager
from pathlib import Path

# Third-party imports
import flask

BASEPATH = Path.home() / '.local/share/autopill'

EPOCH = datetime.datetime(1970, 1, 1, 0, 0, 0)

log = logging.getLogger(__name__)


def render_template_args():
    return dict(
        ctime=time.time()
    )

def render_template(*args, **kwargs):
    nkwargs = render_template_args()
    nkwargs.update(kwargs)
    return flask.render_template(*args, **nkwargs)

def render_template_string(*args, **kwargs):
    nkwargs = render_template_args()
    nkwargs.update(kwargs)
    return flask.render_template_string(*args, **nkwargs)

RX_DIGITS = re.compile(r'\d+')

def version_expand(text):
    return RX_DIGITS.sub(lambda m: m.group(0).rjust(6, '0'), text)

def version_compact(text):
    return RX_DIGITS.sub(lambda m: m.group(0).lstrip('0') or '0', text)

def hsv_color(h, s, v):
    r, g, b = colorsys.hsv_to_rgb(h, s, v)
    r = int(r * 255)
    g = int(g * 255)
    b = int(b * 255)
    return f'#{r:02X}{g:02X}{b:02X}'

def duration_time(duration):
    duration //= 1000

    seconds = duration % 60
    duration //= 60

    minutes = duration % 60
    hours = duration // 60

    if hours:
        return f'{hours}:{minutes:02d}:{seconds:02d}'
    return f'{minutes:d}:{seconds:02d}'

def retry(log, retries, wait, func, *a, **kw):
    while True:
        try:
            return func(*a, **kw)
        except Exception:
            if retries > 0:
                if log:
                    log.exception(f'Failed to run {func.__name__}, will try {retries} more time{"" if retries == 1 else "s"}')
                retries -= 1
                time.sleep(wait)
            else:
                raise

def parsedate(datestr):
    if not datestr:
        return datestr
    dt, _, add = datestr.partition(',')
    if dt == 'today':
        dt = datetime.date.today()
    else:
        y, m, d = map(int, dt.split('-'))
        dt = datetime.date(y, m, d)

    if add:
        dt += datetime.timedelta(days=int(add))

    return dt

class Closable:
    def close(self):
        pass

    def __enter__(self):
        return self

    def __exit__(self, *a):
        self.close()

    def __del__(self):
        self.close()
