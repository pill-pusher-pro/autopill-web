'''
flask_livelog
=============

API for retrieving live logs
'''

from flask import request
from flask_main import app
from flask_utils.livelog import get_lines_after
from flask_utils.utils import tojson

@app.route('/livelog/logs')
def get_livelog():
    since = int(request.args.get('since', '0'))
    result = {
        'log': get_lines_after(since)
    }
    return tojson(result)
